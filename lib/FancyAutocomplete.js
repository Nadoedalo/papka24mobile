/*fixme should have description*/
var FancyAutocomplete = function(mainArr, target, keyObject, wait){
    this.className = "fancyAutoComplete";
    this.mainArr = mainArr || []; /*FIXME should be {arr : [{}, {}], scheme : []} or [] no need for keyObject*/
    this.target = target;
    this.keys = keyObject.renderKeys;
    this.valueKeys = keyObject.valueKeys || this.keys;
    this.searchKeys = keyObject.searchKeys || this.keys;
    this.currentPosition = 1;
    this.foundArr = [];
    this.foundArrLength = 0;
    this.selectedValues = [];
    this.hasClass = function(el, name){
        if(typeof el == "undefined" || el == null){
            return false;
        }
        return new RegExp('(\\s|^)' + name + '(\\s|$)').test(el.className);
    };
    if(this.hasClass(this.target, this.className)){ /*FIXME old instance should be uninitialized*/
        return;
    }

    if(!this.mainArr.length){
        throw new Error('Main array should be specified');
    }
    if(!this.target){
        throw new Error('Target should be specified');
    }
    this.parentTarget = function(){
        return this.target.parentElement;
    };

    this.addWraps = function(){
        this.autocompleteWrap = this.parentTarget().querySelector('.autoCompleteWrap');
        this.addClass(this.target, this.className);
        if(!this.autocompleteWrap){
            this.autocompleteWrap = document.createElement('div');
            this.addClass(this.autocompleteWrap, 'autoCompleteWrap');
            this.parentTarget().appendChild(this.autocompleteWrap);
        }else{
            this.autocompleteWrap.innerHTML = '';
        }
    };
    this.setCurrentPosition = function(position){
        if(position > this.foundArrLength){
            this.currentPosition = 1;
        }else if(position < 1){
            this.currentPosition = this.foundArrLength;
        }else{
            this.currentPosition = position;
        }
    };
    this.moveActiveElement = function(flag){
        flag = !flag; // true - scroll, false - don't scroll
        var new_element = this.autocompleteWrap.querySelector('div:nth-child('+this.currentPosition+')');
        this.rmJSactive(this.autocompleteWrap.querySelector('.js-active'));
        this.addJSactive(new_element);
        if(flag){
            new_element.scrollIntoView(false);
        }
    };
    this.handleKeyUps = function(e){
        var res = false, //preventDefault flag
            keyCode = e.keyCode,
            flag = this.hasClass(this.target, 'js-active');
        if(keyCode === 38 || keyCode === 40){
            //do nothing - handled in handleKeyDowns
        }else if((keyCode === 9 || keyCode === 13)) { //handle TAB && ENTER
            if(flag){
                this.setTargetValue(this.autocompleteWrap.querySelector('.js-active').getAttribute('data-value'));
            }else{
                this.callbackSearch();
            }

        }else{ //any other key - let it be
            var temp = this.setSelectedValues();
            if(!temp){
                this.callbackSearch();
            }
            this.search();
            res = true;
        }
        if(!res && e.preventDefault){
            e.preventDefault();
        }
        return res;
    };
    this.handleKeyDowns = function(e){
        if(!this.hasClass(this.target, 'js-active')){return false;}
        var knownCodes = [38,40,9,13,27],
            keyCode = e.keyCode;
        if(knownCodes.indexOf(keyCode) != -1){
            e.preventDefault();
            e.stopPropagation();
            if(keyCode === 38){ // handle UP arrow
                this.setCurrentPosition(--this.currentPosition);
                this.moveActiveElement();
            }else if(keyCode === 40){ //handle DOWN arrow
                this.setCurrentPosition(++this.currentPosition);
                this.moveActiveElement();
            }
        }
    };
    this.handleKeyPress = function(e){
        var temp;
        if(e.charCode === 44){ //comma
            temp = this.setSelectedValues();
            this.callbackSearch(temp);
        }
    };
    /*fixme rewrite fillAutocompleteWrap
        this.getValuesFromKeys = function(obj){
            if(typeof obj === 'string'){return obj;}
            var res = [];
            foreach(obj, function (v, i) {
                if(this.valueKeys.indexOf(i) != -1){
                    res.push(v);
                }
            }, this);
            return res.join(' ');
        };
    */
    this.getValuesFromKeys = function(obj){
        if(typeof obj === 'string'){return obj;}
        var res = [],
            temp;
        for(temp in obj){
            if(this.valueKeys.indexOf(temp) != -1){
                res.push(obj[temp]);
            }
        }
        return res.join(' ').toLowerCase();
    };
    this.fillAutocompleteWrap = function(arr, el, keys, searchQuery){
        if(!arr || arr.length === 0){return false;}
        var node = document.createElement('div'),
            element = arr.shift(),
            innerHTML = '',
            index,
            temp;
        keys = _.isArray(keys) ? keys : [keys];
        if(keys.length && keys[0]){ //non-empty array
            node.setAttribute('data-value',
                this.getValuesFromKeys(element)
            );
            for(var i = 0, length = keys.length; i < length; i++){
                temp = element[keys[i]];
                index = temp.toLowerCase().indexOf(searchQuery.toLowerCase());
                if(index != -1){
                    temp = temp.substring(0, index)+'<b>'+temp.substring(index, index+searchQuery.length)+'</b>' + temp.substring(index+searchQuery.length, temp.length);
                }
                innerHTML += '<p>'+temp+'</p>';
            }
        }else{ // keys = falsy or empty
            node.setAttribute('data-value', JSON.stringify(element));
            innerHTML = JSON.stringify(element);
            innerHTML = innerHTML.replace(/^"(.*)"$/, '$1');
            index = innerHTML.indexOf(searchQuery);
            innerHTML = innerHTML.substring(0, index)+'<b>'+innerHTML.substring(index, index+searchQuery.length)+'</b>' + innerHTML.substring(index+searchQuery.length, innerHTML.length);
        }
        node.innerHTML = innerHTML;
        node.insertBefore(this.addImage(element.email), node.childNodes[0]); //FIXME отрефакторить, должно приходить параметром)
        el.appendChild(node);
        this.fillAutocompleteWrap(arr, el, keys, searchQuery);
    };
    this.addImage = function(email){ //FIXME отрефакторить, должно приходить параметром
        return this.buildNode("IMG", {src: "/cdn/avatars/" + Sha256.hash(email) + ".png"}, null, {
            error: function () {
                this.error = null;
                this.src = "https://secure.gravatar.com/avatar/" + MD5(email) + "?d=mm";
            }
        });
    };
    this.initListeners = function(){
        this.addEvent(target, 'focus', function(){
            this.search();
        }.bind(this));
        this.addEvent(target, 'blur', function(){
            this.rmJSactive();
        }.bind(this));
        this.addEvent(target, 'keyup', this.handleKeyUps.bind(this));
        this.addEvent(target, 'keydown', this.handleKeyDowns.bind(this));

        this.addEvent(target, 'keypress', this.handleKeyPress.bind(this));
        this.addEvent(this.autocompleteWrap, 'mousedown', function(e){
            var autoCompleteTarget = e.target;
            while(autoCompleteTarget != this.autocompleteWrap){
                if(autoCompleteTarget.tagName.toLocaleLowerCase() === 'div'){
                    break;
                }
                autoCompleteTarget = autoCompleteTarget.parentNode;
            }
            if(autoCompleteTarget.className != this.autocompleteWrap.className){
                e.stopPropagation();
                this.setTargetValue(autoCompleteTarget.getAttribute('data-value'));
            }
            return false;
        }.bind(this));
        this.addEvent(this.autocompleteWrap, 'mouseover', function(e){
            var autoCompleteTarget = e.target,
                index = 1;
            while(autoCompleteTarget != this.autocompleteWrap){
                if(autoCompleteTarget.tagName.toLocaleLowerCase() === 'div'){
                    break;
                }
                autoCompleteTarget = autoCompleteTarget.parentNode;
            }
            while(autoCompleteTarget = autoCompleteTarget.previousElementSibling){
                ++index;
            }
            this.setCurrentPosition(index);
            this.moveActiveElement(true);
        }.bind(this));
    };
    this.setTargetValue = function(value){
        var string = this.target.value,
            arr,
            split = string.split(','),
            last = split[split.length - 1],
            index = string.indexOf(last, string.lastIndexOf(','));
        this.target.value  = (string.substring(0, index) + ' ' + value + ', ').replace(/^\s*/, '');
        arr = this.setSelectedValues();
        this.focusTarget();
        this.callbackSearch(arr);
        return arr.length ? arr : false; //returns array of inserted values or false
    };
    this.callbackSearch = function(arr){
        //TODO should have default callbackSearch of some kind
    };
    this.focusTarget = function(){
        /*FIXME setTimeout on focus is bad, imo*/
        this.search();
        setTimeout(function(){
            this.setCaretPosition(this.target, this.target.value.length);
            this.target.focus();
        }.bind(this), 0);
    };
    this.search = function(){
        var searchQuery = this.target.value.toLowerCase().split(','),
            last = searchQuery[searchQuery.length - 1].replace(/^["\s]+|["\s]+$/g, ''),
            i, length,
            temp;
        if(last){
            for(i = 0, length = this.mainArr.length; i < length; i++){
                if(this.keys){
                    temp = this.searchTroughKeys(this.mainArr[i], last);
                }else{
                    temp = this.mainArr[i].indexOf(last) != -1 && this.compareWithSelectedValues(this.mainArr[i]) ? this.mainArr[i] : false;
                }
                if(temp){
                    this.foundArr.push(temp);
                }
            }
            if(this.foundArr.length){
                this.autocompleteWrap.innerHTML = '';
                this.foundArrLength = this.foundArr.length;
                this.fillAutocompleteWrap(this.foundArr, this.autocompleteWrap, this.keys, last); /*will empty foundArr*/
                this.addJSactive();
                this.currentPosition = 1;
                this.addJSactive(this.autocompleteWrap.querySelector('div:nth-child('+this.currentPosition+')'));
            }else{
                this.rmJSactive();
            }
        }else{
            this.rmJSactive()
        }
    };
    this.addJSactive = function(target){
        this.addClass(target || this.target, 'js-active');
    };
    this.rmJSactive = function(target){
        this.rmClass(target || this.target, 'js-active');
    };
    this.searchTroughKeys = function(obj, comparator){
        comparator = comparator.toLowerCase();
        var temp,
            length,
            flag = false;
        for(temp = 0, length = this.searchKeys.length; temp < length; temp++){
            if(obj[this.searchKeys[temp]] && obj[this.searchKeys[temp]].toLowerCase().indexOf(comparator) != -1 && this.compareWithSelectedValues(obj)){
                flag = true;
                break;
            }
        }
        return flag ? obj : false;
    };
    this.setSelectedValues = function(){
        var arr = this.target.value.split(',').map(function(item){
                return item.replace(/^["\s]+|["\s]+$/g, '');
            }),
            length = arr.length,
            i,
            res = [];
        for(i = 0; i < length; ++i){ //_.compact
            if(arr[i]){
                res.push(arr[i]);
            }
        }
        this.selectedValues = res.length ? res : '';
        return res.length ? res : '';
    };
    this.compareWithSelectedValues = function(obj){
        var value = this.getValuesFromKeys(obj),
            selectedValues = this.selectedValues || [],
            i, length,
            res = true;
        for(i = 0, length = selectedValues.length; i < length; i++){
            if(this.selectedValues[i] && this.selectedValues[i].toLowerCase().indexOf(value) != -1){
                res = false;
                break;
            }
        }
        return res;
    };
    /*some util.js parts*/
    this.addClass = function(el, name) {
        if(typeof el == "undefined" || el == null){
            return false;
        }
        if (!this.hasClass(el, name)) {
            el.className += (el.className ? ' ' : '') + name;
        }
    };
    this.rmClass = function(el, name) {
        if(el === void 0 || el == null){
            return false;
        }
        if (this.hasClass(el, name)) {
            el.className = el.className.replace(name, "").trim();
        }
    };
    this.setCaretPosition = function(element, start, end) {
        if (!end) {
            end = start;
        }
        if (element.setSelectionRange)
            element.setSelectionRange(start, end);
        else if (element.createTextRange) {
            var range = element.createTextRange();
            range.collapse(true);
            range.moveStart('character', start);
            range.moveEnd('character', end);
            range.select();
        }
    };
    var buildCache = [];
    this.buildNode = function(nodeName, attributes, content, events) {
        var element;
        if (!(nodeName in buildCache)) {
            buildCache[nodeName] = document.createElement(nodeName);
        }
        element = buildCache[nodeName].cloneNode(false);

        if (attributes != null) {
            for (var attribute in attributes) {
                if (attributes.hasOwnProperty(attribute)) {
                    if (attribute == "style" && typeof attributes["style"] !== 'string') {
                        var sts = attributes["style"];
                        for (var s in sts) {
                            if (sts.hasOwnProperty(s)) {
                                try {
                                    element.style[s] = sts[s];
                                } catch (e) {
                                }
                            }
                        }
                    } else {
                        element[attribute] = attributes[attribute];
                    }
                }
            }
        }
        if (content != null) {
            if (typeof(content) == 'object') {
                if (content.constructor == Array) {
                    for (var c in content) {
                        if (content.hasOwnProperty(c) && content[c]) {
                            if (typeof(content[c]) == 'object') {
                                element.appendChild(content[c]);
                            } else {
                                element.appendChild(document.createTextNode(content[c]));
                            }
                        }
                    }
                } else {
                    element.appendChild(content);
                }
            } else {
                element.innerHTML = content;
            }
        }
        if (events != null) {
            for (var e in events) {
                if (events.hasOwnProperty(e) && typeof events[e] === "function") {
                    this.addEvent(element, e, events[e]);
                }
            }
        }
        return element;
    };
    this.addEvent = function(el, type, eventHandle) {
        if(el === void 0 || el == null){
            return;
        }
        if (el.addEventListener) {
            el.addEventListener(type, eventHandle, false);
        } else if (el.attachEvent) {
            el.attachEvent("on" + type, eventHandle);
        } else {
            el["on" + type] = eventHandle;
        }
    };
    /*
     * Actual logic is here
     * */
    this.initialize = function(){
        this.addWraps();
        this.initListeners();
    };
    if(!wait){
        this.initialize();
    }
    return this;
};