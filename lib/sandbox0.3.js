/*global
    self, _, Polyglot
*/
(function(self){
    "use strict";
    self.addEventListener('message', function(e){
        var data = JSON.parse(e.data),
            glot,
            template;
        glot = new Polyglot();
        glot.extend(data.translate);
        _.extend(data.data, {polyglot: glot});
        template = _.template(data.template)(data.data);
        self.postMessage({
            template : template,
            cid : data.cid,
            promiseId : data.promiseId
        });
    });
    self.importScripts('/lib/polyglot.min.js', '/lib/underscore.1.8.3.js');
    return self;
}(self));
