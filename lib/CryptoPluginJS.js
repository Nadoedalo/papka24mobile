//-->define("CryptoPluginJS", function(){
if (typeof CryptoPluginJS === "undefined") {

    window.CryptoPluginJS = {
        isInited: false,
        config: {
            browserRegexp: {
                kindle: /(kindle)\/([\w.]+)/,
                avant: /avant\sbrowser?[\/\s]?([\w.]*)/,
                chromium: /(chromium)\/([\w.-]+)/,
                skyfire: /(skyfire)\/([\w.-]+)/,
                vivaldi: /(vivaldi)\/([\w.-]+)/,
                yandex: /(yabrowser)\/([\w.]+)/,
                ucbrowser: /(uc\s?browser)[\/\s]?([\w.]+)/,
                firefox: /(firefox)\/([\w.-]+)/,
                netscape: /(navigator|netscape)\/([\w.-]+)/,
                coast: /(coast)\/([\w.]+)/,
                opera: /(op(era|r|ios))[\/\s]+([\w.]+)/,
                msie: /(?:ms|\()(ie)|((trident).+rv[:\s]([\w.]+).+like\sgecko)|(edge)\/((\d+)?[\w.]+)/,
                androidBrowser: /android.+version\/([\w.]+)\s+(?:mobile\s?safari|safari)/,
                safari: /version\/([\w.]+).+?(mobile\s?safari|safari)/,
                chrome: /(chrome|crmo|crios)\/([\w.]+)/,
                spartan: /edge\/12/
            }
        },
        connect: (function () {

            return function (successCallback, errorCallback) {
                window.location.hash = "#js";

                if (typeof CryptoPluginJS === 'undefined') {
                    CryptoPluginJS.plugin = {};
                }
                var instance = {"pluginType": "", "version": ""};
                updateInstanceInfo(instance);
                CryptoPluginJS.plugin = new CryptoPluginJsConstructor(instance);

                CryptoPluginJS.config.debugFunction = function(text, type){
                    if(type === "info"){
                        console.log(text);
                    } else {
                        console.warn(text);
                    }
                };

                this.isInited = true;
                try {
                    var API = CryptoPluginJS.plugin.send('{"function":"getAPI"}', successCallback, errorCallback);
                    CryptoPluginJS.API = JSON.parse(JSON.parse(API).answer.api);
                    fillFunctions(instance);
                } catch (e) {
                    if (typeof errorCallback === 'function') {
                        errorCallback({
                            code: 0,
                            message: "Плагин не обнаружен или заблокирован",
                            source: "connect"
                        });
                        errorCallback = null;
                        return;
                    }
                }
                execute("getPluginInfo", null, null, function (result) {
                    updateInstanceInfo(instance, result);
                    execute("getVersion", null, null, function (result) {
                        if (result && result.hasOwnProperty("version")) {
                            instance.version = result.version;
                        }
                        if (typeof successCallback === 'function') {
                            successCallback(instance);
                        }
                    }, null);
                }, null);
            };

            /**
             * Обработка ответов от плагина и вызов callback функций
             * @param json ответ в виде объекта
             * @param instance интанс для обновления контекста сессии, если отсутствует, контекст не обновляется
             * @param successCallback callback функция для передачи успешного результата
             * @param errorCallback callback функция для передачи ошибки
             * @param source название функции источника ответа
             */
            function processingAnswer(json, instance, successCallback, errorCallback, source) {
                /** @namespace {String} json.type */
                /** @namespace {Object} json.answer */
                /** @namespace {Number} json.answer.errorCode */
                /** @namespace json.answer.errorText */
                if (json.type === "success") {

                    // Вычисление задержки
                    var delay = ((new Date()).getTime() - CryptoPluginJS.plugin.time) / 1000;
                    // Обработка нормального завершения команды
                    if (json.hasOwnProperty('answer')) {
                        // Обработка ответа.
                        if (typeof successCallback === "function") {
                            if (typeof CryptoPluginJS.config.debugFunction === "function" && source != "getSessionInfo") {
                                CryptoPluginJS.config.debugFunction("P1(" + delay + "s)✈ " + JSON.stringify(json.answer), "info");
                            }
                            if (CryptoPluginJS.plugin.pluginType === "JS" && instance != null && instance.functionLevel > 0) {
                                execute("getSessionInfo", null, instance, function () {
                                    return function (result) {

                                        updateInstanceInfo(instance, result);
                                        setTimeout(function () {
                                            successCallback(json.answer);
                                        }, 0);
                                    }
                                }(), null);
                            } else {
                                setTimeout(function () {
                                    successCallback(json.answer);
                                }, 0);

                            }
                        } else {
                            if (typeof CryptoPluginJS.config.debugFunction === "function" && source != "getSessionInfo") {
                                CryptoPluginJS.config.debugFunction("P2(" + delay + "s)→ " + JSON.stringify(json.answer), "info");
                            }
                            return json.answer;
                        }
                    } else {
                        if (typeof successCallback === "function") {
                            if (typeof CryptoPluginJS.config.debugFunction === "function" && source != "getSessionInfo") {
                                CryptoPluginJS.config.debugFunction("P3(" + delay + "s)✈ [without arguments]", "info");
                            }
                            if (instance != null && instance.functionLevel > 0) {
                                execute("getSessionInfo", null, instance, function () {
                                    return function (result) {
                                        updateInstanceInfo(instance, result);
                                        setTimeout(function () {
                                            successCallback(json.answer);
                                        }, 0);
                                    }
                                }(), function () {
                                    return function () {
                                        updateInstanceInfo(instance, {"sessionState": ""});
                                        setTimeout(function () {
                                            successCallback(json.answer);
                                        }, 0);
                                    }
                                }());
                            } else {
                                setTimeout(function () {
                                    successCallback(json.answer);
                                }, 0);
                            }
                        } else {
                            if (typeof CryptoPluginJS.config.debugFunction === "function" && source != "getSessionInfo") {
                                CryptoPluginJS.config.debugFunction("P4(" + delay + "s)→ [without arguments]", "info");
                            }
                        }
                    }
                } else if (json.type === "error" || json.type === "message") {
                    if ((source === "getSessionInfo" && json.answer.errorCode === 2)) {
                        if (instance != null) {
                            updateInstanceInfo(instance, {sessionState: ""});
                        }
                    } else if (typeof CryptoPluginJS.config.debugFunction === "function" && source != "getSessionInfo") {
                        CryptoPluginJS.config.debugFunction("✗1 code = " + json.answer.errorCode + (json.answer.hasOwnProperty("errorText") ? ", message = " + json.answer.errorText : "") + (typeof source !== "undefined" ? ", source = " + source : ""), "warn");
                    }

                    if (source === "restoreSession") {
                        updateInstanceInfo(instance, {sessionState: ""});
                    }

                    // Обновивить контекст сессии
                    if (instance != null && instance.functionLevel > 0) {
                        //if (CryptoPluginJS.plugin.pluginType === "NM") {
                        execute("getSessionInfo", null, instance, function (result) {
                            updateInstanceInfo(instance, result);
                            if (typeof errorCallback === "function") {
                                errorCallback({
                                    code: json.answer.errorCode,
                                    message: (json.answer.hasOwnProperty("errorText") ? json.answer.errorText : ""),
                                    source: source
                                });
                            }
                        }, null);
                    } else if (typeof errorCallback === "function") {
                        errorCallback({
                            code: json.answer.errorCode,
                            message: (json.answer.hasOwnProperty("errorText") ? json.answer.errorText : ""),
                            source: source
                        });
                    }
                }
            }

            /**
             * Модуль для отправки команд непосредственно в plugin
             * @param {String} actionName - имя команды
             * @param {Object} params - параметры на первом или втором уровне в виде объекта
             * @param {Object} instance - js instance плагина
             * @param {function(Object)} successCallback
             * @param {function(Object)} errorCallback
             */
            function execute(actionName, params, instance, successCallback, errorCallback) {
                setTimeout(function () {
                    var sessionId = "";
                    if (CryptoPluginJS.plugin != null && typeof CryptoPluginJS.plugin.send !== "undefined") {
                        // TODO: test without this methods

                        if (CryptoPluginJS.config.enableWarning && console.warn) {
                            if (typeof successCallback != 'function' && successCallback != null) {
                                if (typeof successCallback === 'undefined') {
                                    console.warn("successCallback isn't set in " + actionName);
                                } else {
                                    console.warn("successCallback isn't function in " + actionName);
                                }
                            }
                            if (typeof errorCallback != 'function' && errorCallback != null) {
                                if (typeof errorCallback === 'undefined') {
                                    console.warn("errorCallback isn't set in " + actionName);
                                } else {
                                    console.warn("errorCallback isn't function in " + actionName);
                                }
                            }
                        }
                        // Формирование команды на выполнение
                        if (typeof instance !== "undefined" && instance != null && instance.hasOwnProperty("sessionId") && instance.sessionId != null && instance.sessionId != "") {
                            sessionId = instance.sessionId;
                        }

                        var command = null;
                        if (typeof params !== "undefined" && params != null && params != "") {
                            command = {"function": actionName, "params": params, "sessionId": sessionId};
                        } else {
                            command = {"function": actionName, "sessionId": sessionId};
                        }

                        // Создание дубликата класса и маскирование паролей перед логированием
                        var logCommand = JSON.parse(JSON.stringify(command));
                        if (typeof logCommand.params !== "undefined") {
                            delete logCommand.url;
                            for (var param in logCommand.params) {
                                if (logCommand.params.hasOwnProperty(param) && param.toLowerCase().indexOf("password") != -1) {
                                    logCommand.params[param] = new Array(logCommand.params[param].length).join('*');
                                }
                            }
                        }

                        if (typeof CryptoPluginJS.config.debugFunction === "function" && actionName != "getSessionInfo") {

                            CryptoPluginJS.config.debugFunction("P5← " + JSON.stringify(logCommand), "info");
                        }

                        if (actionName !== "getSessionInfo") {
                            // Запоминание времени для определения задержки выполнения команды
                            CryptoPluginJS.plugin.time = (new Date()).getTime();
                        }
                        var json = null;

                        var inst = null;
                        if (actionName !== "getSessionInfo" && typeof result !== "undefined" && result && typeof instance !== "undefined" && instance && instance.hasOwnProperty("sessionId") && instance.sessionId != "") {
                            inst = instance;
                        }

                        command.url = document.URL;

                        var commandTest = JSON.stringify(command);

                        // Выполнение команды
                        result = CryptoPluginJS.plugin.send(commandTest, successCallback, errorCallback);
                        try {
                            json = JSON.parse(result);
                        } catch (ex) {
                            if (typeof CryptoPluginJS.config.debugFunction === "function" && actionName != "getSessionInfo" && actionName != 'selectFile') {
                                CryptoPluginJS.config.debugFunction("Непредсказуемое поведение плагина.", "warn");
                            }
                            return;
                        }

                        return processingAnswer(json, inst, successCallback, errorCallback, actionName);

                    } else {
                        if (typeof errorCallback === 'function') {
                            errorCallback({
                                code: 0,
                                message: "Плагин не обнаружен или заблокирован",
                                source: actionName
                            });
                            errorCallback = null;
                        }
                    }
                }, 10);
            }

            /**
             * Динамическое заполнение доступных методов из справочника.
             */
            function fillFunctions(instance) {
                if (CryptoPluginJS.hasOwnProperty("API")) {
                    for (var i = CryptoPluginJS.API.length - 1; i >= 0; i--) {
                        var apiFunction = CryptoPluginJS.API[i];
                        // Отфильтрованы методы с других уровней.
                        if (!instance.hasOwnProperty(CryptoPluginJS.API[i].name)) {
                            // Добавить реализацию нужного метода.
                            instance[apiFunction.name] = function () {
                                var func = apiFunction;
                                var p;
                                return function () {
                                    var params = {}, hasParam = false;

                                    if (func.hasOwnProperty("params")) {
                                        if (arguments.length > func.params.length + 2) {
                                            if (typeof CryptoPluginJS.config.debugFunction === "function") {
                                                CryptoPluginJS.config.debugFunction("Передано больше параметров чем нужно для функции " + func.name, "warn");
                                            }
                                        } else {
                                            var countReqParams = 0;
                                            for (p = 0; p < func.params.length; p++) {
                                                if (!func.params[p].hasOwnProperty("optional") || !func.params[p]["optional"]) {
                                                    countReqParams++;
                                                }
                                            }
                                            if (typeof CryptoPluginJS.config.debugFunction === "function") {
                                                if (arguments.length >= 2 && typeof arguments[arguments.length - 2] === "function" && countReqParams + 2 > arguments.length) {
                                                    CryptoPluginJS.config.debugFunction("Передано меньше параметров чем нужно для функции " + func.name, "warn");
                                                }
                                                if (arguments.length >= 1 && typeof arguments[arguments.length - 1] === "function" && countReqParams + 1 > arguments.length) {
                                                    CryptoPluginJS.config.debugFunction("Передано меньше параметров чем нужно для функции " + func.name, "warn");
                                                }
                                            }
                                        }

                                    } else if (arguments.length > 2 && typeof CryptoPluginJS.config.debugFunction === "function") {
                                        CryptoPluginJS.config.debugFunction("Передано больше параметров чем нужно для функции " + func.name, "warn");
                                    }

                                    var normalCallback = null;
                                    var errorCallback = null;

                                    // Обработка обычных функций
                                    if (func.hasOwnProperty("params")) {
                                        for (p = 0; p < arguments.length; p++) {
                                            if (typeof arguments[p] === "function") {
                                                if (p === arguments.length - 2 && typeof arguments[p + 1] === "function") {
                                                    normalCallback = arguments[p];
                                                    errorCallback = arguments[p + 1];
                                                    break;
                                                } else if (p === arguments.length - 1) {
                                                    normalCallback = arguments[p];
                                                    break;
                                                } else if (typeof CryptoPluginJS.config.debugFunction === "function") {
                                                    // Ошибочное положения функций в параметрах.
                                                    CryptoPluginJS.config.debugFunction("Функции обратного вызова переданы в неправильных позициях " + func.name, "warn");
                                                }
                                            } else {
                                                // Конвертировать параметры
                                                if (func.params[p].hasOwnProperty("type")) {
                                                    var type = func.params[p].type;
                                                    if (type === "Number") {
                                                        arguments[p] = parseInt(arguments[p]);
                                                        if (isNaN(arguments[p])) {
                                                            continue;
                                                        }
                                                    } else if (type === "Boolean" && typeof arguments[p] !== 'boolean') {
                                                        if (arguments[p] === "") {
                                                            continue;
                                                        }
                                                        arguments[p] = arguments[p].toLowerCase() === "true";
                                                    }
                                                }
                                                hasParam = true;
                                                if (arguments[p] != null) {
                                                    params[func.params[p].name] = arguments[p];
                                                }
                                                if (func.params[p].name === "sessionId" && func.name === "restoreSession") {
                                                    instance.sessionId = arguments[p];
                                                }
                                            }
                                        }
                                    }
                                    if (!hasParam) {
                                        params = null;
                                    }

                                    var callback = null;

                                    if (func.name === "updateInfo") {
                                        execute("getPluginInfo", null, null, function (result) {

                                            updateInstanceInfo(instance, result);
                                            if (instance.sessionId != null && instance.sessionId.length > 0) {
                                                execute("getSessionInfo", null, instance, function (result) {
                                                    updateInstanceInfo(instance, result);
                                                    normalCallback();
                                                }, function (error) {
                                                    if (error.code === 2) {
                                                        updateInstanceInfo(instance);
                                                        if (typeof normalCallback === "function") {
                                                            normalCallback();
                                                        }
                                                    } else {
                                                        if (typeof errorCallback === "function") {
                                                            errorCallback();
                                                        }
                                                    }
                                                });
                                            } else {
                                                if (typeof normalCallback === "function") {
                                                    normalCallback();
                                                }
                                            }
                                        }, errorCallback);
                                        return;
                                    }

                                    if (func.name === "openSession" || func.name === "restoreSession") {
                                        callback = function () {
                                            return function (result) {
                                                updateInstanceInfo(instance);
                                                if (typeof instance !== "undefined" && typeof result !== "undefined" && result && instance && instance.hasOwnProperty("sessionId") && instance.sessionId != "" && result.hasOwnProperty("sessionId")) {
                                                    instance.sessionId = result.sessionId;
                                                }
                                                execute("getPluginInfo", null, null,
                                                    function (result) {
                                                        updateInstanceInfo(instance, result);
                                                        execute("getSessionInfo", null, instance,
                                                            function (result) {
                                                                updateInstanceInfo(instance, result);
                                                                normalCallback(instance.sessionId);
                                                            }, errorCallback);
                                                    }, errorCallback);
                                                return result;
                                            }
                                        }();
                                    } else {
                                        callback = normalCallback;
                                    }

                                    // Выполнить метод c обратным вызовом и обработкой обработкой ошибки.
                                    return execute(func.name, params, instance, callback, errorCallback);
                                }
                            }()
                        }
                    }
                }
            }

            /**
             * Обновление инфомрации инстанса.
             * Принимаются данные от getSessionInfo и getPluginInfo
             */
            function updateInstanceInfo(instance, json) {
                if (typeof instance === "undefined" || instance === null) {
                    return;
                }

                if (typeof json === "undefined") {
                    // Инициализация состояния плагина
                    instance.storageType = 0;
                    instance.functionLevel = 0;
                    instance.activeSessionCount = 0;
                    instance.sessionId = null;
                    instance.sessionExpiryTime = null;
                    instance.sessionState = "absent";
                    instance.filePath = null;
                    // instance.deviceId = null;
                    instance.storageType = null;
                    instance.storageSpec = null;
                    instance.storageVersion = null;
                    // instance.deviceDriverVersion = null;
                    // instance.deviceModel = null;
                    // instance.deviceName = null;
                    // instance.deviceSerialNumber = null;
                    // instance.deviceState = null;
                    // instance.deviceHasPuk = null;
                    instance.keyType = null;
                    instance.keyAlias = null;
                    instance.fileModified = null;
                } else {
                    for (var el in json) {
                        if (json.hasOwnProperty(el)) {
                            if (json[el] === "" || json[el] == null) {
                                if (instance.hasOwnProperty(el)) {
                                    instance[el] = null;

                                }
                            } else {
                                if (el === "sessionExpiryTime") {
                                    instance[el] = new Date(parseInt(json[el] * 1000));
                                } else {
                                    instance[el] = json[el];
                                }
                            }
                            if (el === "sessionState") {
                                // Обновление статуса сессии
                                switch (json[el]) {
                                    case "":
                                    case "absent":
                                        instance.functionLevel = 0;
                                        instance.sessionId = null;
                                        instance.sessionState = "absent";
                                        break;
                                    case "new":
                                        instance.functionLevel = 1;
                                        break;
                                    case "createStorage":
                                    case "selectModifiedFileStorage":
                                        instance.functionLevel = 2;
                                        break;
                                    case "selectFileStorage":
                                        instance.functionLevel = 3;
                                        break;
                                    case "selectDeviceStorage":
                                        instance.functionLevel = 4;
                                        break;
                                    case "selectKey":
                                        instance.functionLevel = 5;
                                        break;
                                }
                            }
                        }
                    }
                }
            }


            function CryptoPluginJsConstructor(instance) {
                'use strict';

                var data = [];
                var isSessionRestored = false;
                var cryptoniteXErrorIncreaser = 10000;

                var gateConfig = {
                    "sessionState": 'absent',
                    "sessionExpiryTime": '',
                    "storageType": '',
                    "storageSpec": '',
                    "storageVersion": '',
                    "filePath": '',
                    "fileModified": '',
                    "keyType": '',
                    "keyAlias": '',
                    "status": '',
                    "sessionId": '',
                    "sessionTimeDuration": '',
                    "domainWhiteList": '',
                    "time": '',
                    "url": '',
                    "storage": ''
                };

                instance.pluginType = "JS";
                instance.send = function (queryJson, successFunction, failFunction) {
                    var json;
                    try {
                        json = JSON.parse(queryJson);
                    } catch (e) {
                        return jsonCreateErrorStr('send', 1, 'Команда сформирована некорректно.');
                    }

                    if (typeof json.function == 'undefined') {
                        return jsonCreateErrorStr('send', 1, 'Команда сформирована некорректно.');
                    }

                    if (json.function != 'openSession' && json.function != 'getPluginInfo' &&
                        json.function != 'getVersion' && json.function != 'getSessionInfo' &&
                        json.function != 'getAPI' &&
                        json.sessionId != gateConfig.sessionId) {
                        return jsonCreateErrorStr('send', 8, 'Неккоректная сессия.');
                    }

                    var jsonExample = '';
                    if (json.params == 'undefined' || json.params == '') {
                        jsonExample = json.sessionId;
                    } else {
                        jsonExample = json.params;
                    }

                    return eval(json.function)(jsonExample, successFunction, failFunction);
                };

                function jsonCreateErrorStr(sourceString, errorCode, errorString) {
                    return JSON.stringify({
                        "source": sourceString,
                        "type": "error",
                        "answer": {"errorCode": errorCode, "errorText": errorString}
                    });

                }

                function jsonCreateByStrWithoutSpecStr(sourceString, asnwBool) {
                    if (asnwBool === true) {
                        return JSON.stringify({
                            "source": sourceString,
                            "type": "success"
                        });
                    } else {
                        return JSON.stringify({
                            "source": sourceString,
                            "type": "error"
                        });
                    }
                }

                function jsonCreateByStr(sourceString, asnwBool, specificationStr) {

                    if (asnwBool === true) {
                        return JSON.stringify({
                            "source": sourceString,
                            "type": "success",
                            "answer": specificationStr
                        });
                    } else {
                        return JSON.stringify({
                            "source": sourceString,
                            "type": "error",
                            "answer": {"errorCode": 0, "errorText": specificationStr}
                        });
                    }
                }


                function jsonCreateErrorByCode(sourceString, errorCode) {
                    return JSON.stringify({
                        "source": sourceString,
                        "type": "error",
                        "answer": {"errorCode": errorCode, "errorText": "Виникла помилка cryptoniteX."}
                    });
                }

                function gateNullifiedData() {
                    if (typeof data[0] != 'undefined') {
                        gateConfig = JSON.parse(JSON.stringify(data[0]));
                    }
                }

                function getRandomIntBase64(numBytes) {
                    var res = '';
                    for (var i = 0; i < numBytes * 2; i++) {
                        res += ((Math.floor(Math.random() * (10 - 1)) + 1).toString(16));
                    }

                    return res.hexToBase64();
                }

                function getAPI() {
                    // {"name":"saveFileStorage","params":[
                    //     {"name":"filePath","type":"String"},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"forgetKey","params":[
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"deleteKey","params":[
                    //     {"name":"alias","type":"String"},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"getPublicKey","params":[
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"changeKeyAlias","params":[
                    //     {"name":"alias","type":"String"},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"changeKeyPassword","params":[
                    //     {"name":"alias","type":"String"},
                    //     {"name":"oldPassword","type":"String"},
                    //     {"name":"newPassword","type":"String"},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"getAliasByCertificate","params":[
                    //     {"name":"certificate","type":"String"},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"setCertificateChain","params":[
                    //     {"name":"certificateChain","type":"Array"},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"deleteCertificateChain","params":[
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"getCertificateReq","params":[
                    //     {"name":"subject","type":"Array"},
                    //     {"name":"attributes","type":"Array"},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"getCertificateInfo","params":[
                    //     {"name":"certificate","type":"String"},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"getSearchCertificateReq","params":[
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"getOCSPReq","params":[
                    //     {"name":"certificate","type":"String"},
                    //     {"name":"rootCertificate","type":"String"},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"checkOCSPResp","params":[
                    //     {"name":"OCSPResp","type":"String"},
                    //     {"name":"OCSPCertificate","type":"String","optional":true},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"hashData","params":[
                    //     {"name":"data","type":"String"},
                    //     {"name":"hashParams","type":"String","optional":true},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"signHash","params":[
                    //     {"name":"hashData","type":"String"},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"signData","params":[
                    //     {"name":"data","type":"String"},
                    //     {"name":"hashParams","type":"String","optional":true},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"getTimeStampReq","params":[
                    //     {"name":"data","type":"String"},
                    //     {"name":"hashParams","type":"String","optional":true},
                    //     {"name":"certReq","type":"Boolean","optional":true},
                    //     {"name":"reqPolicy","type":"String","optional":true},
                    //     {"name":"nonce","type":"String","optional":true},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"CMSSign","params":[
                    //     {"name":"data","type":"String"},
                    //     {"name":"hashParams","type":"String","optional":true},
                    //     {"name":"certificate","type":"String","optional":true},
                    //     {"name":"timeStamp","type":"String","optional":true},
                    //     {"name":"includeData","type":"Boolean","optional":true},
                    //     {"name":"includeCertificate","type":"Boolean","optional":true},
                    //     {"name":"TSPhashParams","type":"String","optional":true},
                    //     {"name":"TSPcertReq","type":"Boolean","optional":true},
                    //     {"name":"TSPreqPolicy","type":"String","optional":true},
                    //     {"name":"signingTime","type":"Boolean","optional":true},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"CMSSignHash","params":[
                    //     {"name":"hashData","type":"String"},
                    //     {"name":"hashParams","type":"String","optional":true},
                    //     {"name":"certificate","type":"String","optional":true},
                    //     {"name":"timeStamp","type":"String","optional":true},
                    //     {"name":"includeCertificate","type":"Boolean","optional":true},
                    //     {"name":"TSPhashParams","type":"String","optional":true},
                    //     {"name":"TSPcertReq","type":"Boolean","optional":true},
                    //     {"name":"TSPreqPolicy","type":"String","optional":true},
                    //     {"name":"signingTime","type":"Boolean","optional":true},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"CMSEncrypt","params":[
                    //     {"name":"data","type":"String"},
                    //     {"name":"destCertificate","type":"String"},
                    //     {"name":"srcCertificate","type":"String","optional":true},
                    //     {"name":"includeSrcCertificate","type":"Boolean","optional":true},
                    //     {"name":"cryptMode ","type":"String","optional":true},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"CMSDecrypt","params":[
                    //     {"name":"envelopedCMS","type":"String"},
                    //     {"name":"srcCertificate","type":"String","optional":true},
                    //     {"name":"destCertificate","type":"String","optional":true},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"CMSSplit","params":[
                    //     {"name":"CMS","type":"String"},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"CMSJoin","params":[
                    //     {"name":"CMSArray","type":"Array"},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"getDataFromCMS","params":[
                    //     {"name":"CMS","type":"String"},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]},
                    //
                    // {"name":"CMSVerify","params":[
                    //     {"name":"CMS","type":"String"},
                    //     {"name":"data","type":"String","optional":true},
                    //     {"name":"certificateChain","type":"Array","optional":true},
                    //     {"name":"successCallback","type":"Function","optional":true},
                    //     {"name":"errorCallback","type":"Function","optional":true}]}
                    var API =
                        '[{"name":"openSession","params":[\
                            {"name":"sessionTimeDuration","type":"Number","optional":true},\
                            {"name":"domainWhiteList","type":"Array","optional":true},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"restoreSession","params":[\
                            {"name":"sessionId","type":"String"},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"closeSession","params":[\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"closeAllSessions","params":[\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"createFileStorage","params":[\
                            {"name":"storageType","type":"String"},\
                            {"name":"password","type":"String"},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"changeStoragePassword","params":[\
                            {"name":"oldPassword","type":"String"},\
                            {"name":"newPassword","type":"String"},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"selectKey","params":[\
                            {"name":"alias","type":"String"},\
                            {"name":"password","type":"String"},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"selectFileStorage","params":[\
                            {"name":"storageBase64","type":"String"},\
                            {"name":"password","type":"String"},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"saveFileStorage","params":[\
                            {"name":"filePath","type":"String"},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"getKeysList","params":[\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"generateKeyPair","params":[\
                            {"name":"alias","type":"String"},\
                            {"name":"password","type":"String"},\
                            {"name":"signParams","type":"String"},\
                            {"name":"keyInfo","type":"String","optional":true},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"getCertificate","params":[\
                            {"name":"keyUsage","type":"Array","optional":true},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"getCertificateChain","params":[\
                            {"name":"alias","type":"String","optional":true},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"setCertificateChain","params":[\
                            {"name":"certificateChain","type":"Array"},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"getCertificateReq","params":[\
                            {"name":"subject","type":"Array"},\
                            {"name":"attributes","type":"Array"},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"signData","params":[\
                            {"name":"data","type":"String"},\
                            {"name":"hashParams","type":"String","optional":true},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"signHash","params":[\
                            {"name":"hashData","type":"String"},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"CMSSign","params":[\
                            {"name":"data","type":"String"},\
                            {"name":"hashParams","type":"String","optional":true},\
                            {"name":"certificate","type":"String","optional":true},\
                            {"name":"timeStamp","type":"String","optional":true},\
                            {"name":"includeData","type":"Boolean","optional":true},\
                            {"name":"includeCertificate","type":"Boolean","optional":true},\
                            {"name":"TSPhashParams","type":"String","optional":true},\
                            {"name":"TSPcertReq","type":"Boolean","optional":true},\
                            {"name":"TSPreqPolicy","type":"String","optional":true},\
                            {"name":"signingTime","type":"Boolean","optional":true},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"CMSSignHash","params":[\
                            {"name":"hashData","type":"String"},\
                            {"name":"hashParams","type":"String","optional":true},\
                            {"name":"certificate","type":"String","optional":true},\
                            {"name":"timeStamp","type":"String","optional":true},\
                            {"name":"includeCertificate","type":"Boolean","optional":true},\
                            {"name":"TSPhashParams","type":"String","optional":true},\
                            {"name":"TSPcertReq","type":"Boolean","optional":true},\
                            {"name":"TSPreqPolicy","type":"String","optional":true},\
                            {"name":"signingTime","type":"Boolean","optional":true},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                            {"name":"getTimeStampReq","params":[\
                            {"name":"data","type":"String"},\
                            {"name":"hashParams","type":"String","optional":true},\
                            {"name":"certReq","type":"Boolean","optional":true},\
                            {"name":"reqPolicy","type":"String","optional":true},\
                            {"name":"nonce","type":"String","optional":true},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"getCertificateInfo","params":[\
                            {"name":"certificate","type":"String"},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"getOCSPReq","params":[\
                            {"name":"certificate","type":"String"},\
                            {"name":"rootCertificate","type":"String"},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"CMSSplit","params":[\
                            {"name":"CMS","type":"String"},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"CMSJoin","params":[\
                            {"name":"CMSArray","type":"Array"},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]},\
                        {"name":"selectFile","params":[\
                            {"name":"file","type":"String"},\
                            {"name":"successCallback","type":"Function","optional":true},\
                            {"name":"errorCallback","type":"Function","optional":true}]}\
                            ]';
                    return jsonCreateByStr('getAPI', true, {"api": API});
                }


                function findIdxSessionById(sessionId) {
                    var i;

                    if (data.length == 0) {
                        return 0;
                    }
                    for (i = 0; i < data.length; i++) {
                        if (data[i].sessionId === sessionId) {
                            break;
                        }
                    }

                    //TODO: error if > list range
                    return i === data.length ? -1 : i;
                }

                function openSession(json) {

                    if (typeof json.sessionTimeDuration == 'undefined' && typeof json.domainWhiteList == 'undefined') {
                        return jsonCreateErrorStr('openSession', 1, 'Команда сформирована некорректно.');
                    }

                    //TODO: check session id is unique in queue
                    data.push(gateConfig);

                    gateNullifiedData();

                    gateConfig.storage = new CryptoniteX();
                    gateConfig.sessionId = getRandomIntBase64(33);
                    //TODO: check domainWhiteList and sessionTimeDuration
                    gateConfig.sessionTimeDuration = json.sessionTimeDuration;
                    gateConfig.sessionExpiryTime = (new Date().getTime() / 1000) + json.sessionTimeDuration;

                    gateConfig.domainWhiteList = json.domainWhiteList;

                    gateConfig.sessionState = 'new';

                    isSessionRestored = false;

                    return jsonCreateByStr('openSession', true, {"sessionId": gateConfig.sessionId});
                }

                function getPluginInfo() {
                    return jsonCreateByStr('getPluginInfo', true, {"activeSessionCount": data.length - isSessionRestored});
                }

                function getSessionInfo() {

                    //TODO: stringify replacer
                    var getSessionInfoConfig = {
                        "sessionState": 'absent',
                        "sessionExpiryTime": '',
                        "storageType": '',
                        "storageSpec": '',
                        "storageVersion": '',
                        "filePath": '',
                        "fileModified": '',
                        "keyType": '',
                        "keyAlias": ''
                    };

                    for (var el in getSessionInfoConfig) {
                        getSessionInfoConfig[el] = gateConfig[el];
                    }

                    return jsonCreateByStr('getSessionInfo', true, getSessionInfoConfig);
                }

                function getVersion(success) {
                    return jsonCreateByStr('getVersion', true, {"version": "1.2.1"});
                }

                function closeSession(params) {

                    var idx = findIdxSessionById(gateConfig.sessionId);

                    if (gateConfig.sessionId !== '') {
                        var sid = gateConfig.sessionId;
                        gateNullifiedData();
                        if (idx !== -1) {
                            data.splice(idx, 1);
                        }
                        return jsonCreateByStr('closeSession', true, {"sessionId": sid});
                    }

                    return jsonCreateErrorStr('closeSession', 2, 'Сессия не распознана или уже закрыта.');
                }

                function restoreSession(params) {

                    if (typeof params.sessionId === 'undefined') {
                        return jsonCreateErrorStr('changeStoragePassword', 1, 'Команда сформирована некорректно.');
                    }

                    if (findIdxSessionById(gateConfig.sessionId) === -1) {
                        data.push(gateConfig);
                    }

                    var i = findIdxSessionById(params.sessionId);

                    if (i < 0) {
                        return jsonCreateErrorStr('restoreSession', 2, 'Сессия не распознана или уже закрыта.');
                    }

                    gateConfig = data[i];

                    isSessionRestored = true;

                    return jsonCreateByStr('restoreSession', true, {"sessionId": gateConfig.sessionId});
                }

                function closeAllSessions(params) {

                    gateNullifiedData();

                    data = [];

                    return jsonCreateByStr('closeAllSessions', true);
                }

                function changeStoragePassword(params) {
                    //TODO:
                    if (typeof params.oldPassword === 'undefined' || typeof params.newPassword === 'undefined') {
                        jsonCreateErrorStr('changeStoragePassword', 1, 'Команда сформирована некорректно.');
                    }
                    if (gateConfig.sessionState === 'selectFileStorage') {
                        gateConfig.storage.changePassword(params.oldPassword, params.newPassword);
                        if (gateConfig.storage.errorCode != 0) {
                            jsonCreateErrorStr('changeStoragePassword', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser, 'Change password failed.')
                        }
                        jsonCreateByStr('changeStoragePassword', true);
                    }

                    return jsonCreateErrorStr('changeStoragePassword', 4, 'Данный метод недоступен в текущем состоянии сессии.');
                }

                function selectFileStorage(params) {
                    if (typeof params.storageBase64 == 'undefined' || typeof params.password == 'undefined') {
                        return jsonCreateErrorStr('selectFileStorage', 1, 'Команда сформирована некорректно.');
                    }
                    //TODO: opensession check
                    gateConfig.storage.free();
                    if (params.storageBase64 != '') {
                        gateConfig.storage.loadStorage(null, params.storageBase64, params.password);
                    } else {
                        gateConfig.storage.loadStorage(null, CryptoPluginJS.plugin.storageBase64, params.password);
                    }
                    if (gateConfig.storage.errorCode != 0) {
                        return jsonCreateErrorByCode('selectFileStorage', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser);
                    }
                    //TODO: check storage or other
                    gateConfig.storage.storageSpec = 'IIT';

                    gateConfig.sessionState = 'selectFileStorage';

                    return jsonCreateByStrWithoutSpecStr('selectFileStorage', true);
                }

                function saveFileStorage(params) {
                    if (gateConfig.sessionState == 'selectFileStorage' || gateConfig.sessionState == 'selectKey') {
                    var saveAs = saveAs || (function (view) {
                            "use strict";
                            // IE <10 is explicitly unsupported
                            if (typeof view === "undefined" || typeof navigator !== "undefined" && /MSIE [1-9]\./.test(navigator.userAgent)) {
                                return;
                            }
                            var
                                doc = view.document
                                // only get URL when necessary in case Blob.js hasn't overridden it yet
                                , get_URL = function () {
                                    return view.URL || view.webkitURL || view;
                                }
                                , save_link = doc.createElementNS("http://www.w3.org/1999/xhtml", "a")
                                , can_use_save_link = "download" in save_link
                                , click = function (node) {
                                    var event = new MouseEvent("click");
                                    node.dispatchEvent(event);
                                }
                                , is_safari = /constructor/i.test(view.HTMLElement)
                                , is_chrome_ios = /CriOS\/[\d]+/.test(navigator.userAgent)
                                , throw_outside = function (ex) {
                                    (view.setImmediate || view.setTimeout)(function () {
                                        throw ex;
                                    }, 0);
                                }
                                , force_saveable_type = "application/octet-stream"
                                // the Blob API is fundamentally broken as there is no "downloadfinished" event to subscribe to
                                , arbitrary_revoke_timeout = 1000 * 40 // in ms
                                , revoke = function (file) {
                                    var revoker = function () {
                                        if (typeof file === "string") { // file is an object URL
                                            get_URL().revokeObjectURL(file);
                                        } else { // file is a File
                                            file.remove();
                                        }
                                    };
                                    setTimeout(revoker, arbitrary_revoke_timeout);
                                }
                                , dispatch = function (filesaver, event_types, event) {
                                    event_types = [].concat(event_types);
                                    var i = event_types.length;
                                    while (i--) {
                                        var listener = filesaver["on" + event_types[i]];
                                        if (typeof listener === "function") {
                                            try {
                                                listener.call(filesaver, event || filesaver);
                                            } catch (ex) {
                                                throw_outside(ex);
                                            }
                                        }
                                    }
                                }
                                , auto_bom = function (blob) {
                                    // prepend BOM for UTF-8 XML and text/* types (including HTML)
                                    // note: your browser will automatically convert UTF-16 U+FEFF to EF BB BF
                                    if (/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(blob.type)) {
                                        return new Blob([String.fromCharCode(0xFEFF), blob], {type: blob.type});
                                    }
                                    return blob;
                                }
                                , FileSaver = function (blob, name, no_auto_bom) {
                                    if (!no_auto_bom) {
                                        blob = auto_bom(blob);
                                    }
                                    // First try a.download, then web filesystem, then object URLs
                                    var
                                        filesaver = this
                                        , type = blob.type
                                        , force = type === force_saveable_type
                                        , object_url
                                        , dispatch_all = function () {
                                            dispatch(filesaver, "writestart progress write writeend".split(" "));
                                        }
                                        // on any filesys errors revert to saving with object URLs
                                        , fs_error = function () {
                                            if ((is_chrome_ios || (force && is_safari)) && view.FileReader) {
                                                // Safari doesn't allow downloading of blob urls
                                                var reader = new FileReader();
                                                reader.onloadend = function () {
                                                    var url = is_chrome_ios ? reader.result : reader.result.replace(/^data:[^;]*;/, 'data:attachment/file;');
                                                    var popup = view.open(url, '_blank');
                                                    if (!popup) view.location.href = url;
                                                    url = undefined; // release reference before dispatching
                                                    filesaver.readyState = filesaver.DONE;
                                                    dispatch_all();
                                                };
                                                reader.readAsDataURL(blob);
                                                filesaver.readyState = filesaver.INIT;
                                                return;
                                            }
                                            // don't create more object URLs than needed
                                            if (!object_url) {
                                                object_url = get_URL().createObjectURL(blob);
                                            }
                                            if (force) {
                                                view.location.href = object_url;
                                            } else {
                                                var opened = view.open(object_url, "_blank");
                                                if (!opened) {
                                                    // Apple does not allow window.open, see https://developer.apple.com/library/safari/documentation/Tools/Conceptual/SafariExtensionGuide/WorkingwithWindowsandTabs/WorkingwithWindowsandTabs.html
                                                    view.location.href = object_url;
                                                }
                                            }
                                            filesaver.readyState = filesaver.DONE;
                                            dispatch_all();
                                            revoke(object_url);
                                        }
                                        ;
                                    filesaver.readyState = filesaver.INIT;

                                    if (can_use_save_link) {
                                        object_url = get_URL().createObjectURL(blob);
                                        setTimeout(function () {
                                            save_link.href = object_url;
                                            save_link.download = name;
                                            click(save_link);
                                            dispatch_all();
                                            revoke(object_url);
                                            filesaver.readyState = filesaver.DONE;
                                        });
                                        return;
                                    }

                                    fs_error();
                                }
                                , FS_proto = FileSaver.prototype
                                , saveAs = function (blob, name, no_auto_bom) {
                                    return new FileSaver(blob, name || blob.name || "download", no_auto_bom);
                                }
                                ;
                            // IE 10+ (native saveAs)
                            if (typeof navigator !== "undefined" && navigator.msSaveOrOpenBlob) {
                                return function (blob, name, no_auto_bom) {
                                    name = name || blob.name || "download";

                                    if (!no_auto_bom) {
                                        blob = auto_bom(blob);
                                    }
                                    return navigator.msSaveOrOpenBlob(blob, name);
                                };
                            }

                            FS_proto.abort = function () {
                            };
                            FS_proto.readyState = FS_proto.INIT = 0;
                            FS_proto.WRITING = 1;
                            FS_proto.DONE = 2;

                            FS_proto.error =
                                FS_proto.onwritestart =
                                    FS_proto.onprogress =
                                        FS_proto.onwrite =
                                            FS_proto.onabort =
                                                FS_proto.onerror =
                                                    FS_proto.onwriteend =
                                                        null;

                            return saveAs;
                        }(
                            typeof self !== "undefined" && self
                            || typeof window !== "undefined" && window
                            || this.content
                        ));

                    if (typeof module !== "undefined" && module.exports) {
                        module.exports.saveAs = saveAs;
                    } else if ((typeof define !== "undefined" && define !== null) && (define.amd !== null)) {
                        define("FileSaver.js", function () {
                            return saveAs;
                        });
                    }

                    var res = gateConfig.storage.storageToBase64();

                    if (gateConfig.storage.errorCode != 0) {
                        return jsonCreateErrorStr('saveFileStorage', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser, 'Save FileS torage file storage failed.');
                    }

                    function b64toBlob(b64Data, contentType, sliceSize) {
                        contentType = contentType || '';
                        sliceSize = sliceSize || 512;

                        var byteCharacters = atob(b64Data);
                        var byteArrays = [];

                        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                            var slice = byteCharacters.slice(offset, offset + sliceSize);

                            var byteNumbers = new Array(slice.length);
                            for (var i = 0; i < slice.length; i++) {
                                byteNumbers[i] = slice.charCodeAt(i);
                            }

                            var byteArray = new Uint8Array(byteNumbers);

                            byteArrays.push(byteArray);
                        }

                        var blob = new Blob(byteArrays, {type: contentType});
                        return blob;
                    }

                    saveAs(b64toBlob(res, null, null), res.length.toString() + '_storage' + '.pfx');

                }
                    return jsonCreateErrorStr('getKeysList', 4, 'Данный метод недоступен в текущем состоянии сессии.');

                }

                function createFileStorage(params) {
                    //TODO:
                    if (params.storageType == 'undefined' || params.password == 'undefined') {
                        return jsonCreateErrorStr('createFileStorage', 1, 'Команда сформирована некорректно.');
                    }

                    gateConfig.storage.free();

                    //TODO: Количество итераций
                    var storageType;
                    if (params.storageType === 'IIT') {
                        gateConfig.storageType = 'IIT';
                        storageType = 1;
                    } else if (params.storageType === 'JKS') {
                        storageType = 7;
                        gateConfig.storageType = 'JKS';
                    } else {
                        return jsonCreateErrorStr('createFileStorage', 122, 'Не поддерживается создание данного типа хранилища.');
                    }
                    gateConfig.storage.createStorage(storageType, params.password, 2048);
                    if (gateConfig.storage.errorCode != 0) {
                        return jsonCreateErrorStr('createFileStorage', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser, 'Create file storage file storage failed.');
                    }
                    gateConfig.sessionState = 'selectFileStorage';

                    return jsonCreateByStrWithoutSpecStr('createFileStorage', true);
                }

                function getKeysList() {
                    if (gateConfig.sessionState == 'selectFileStorage' || gateConfig.sessionState == 'selectKey') {
                        var keys = gateConfig.storage.enumKeys();
                        var res = [];
                        for (var i = 0; i < keys.length; i++) {
                            res.push({"alias" : keys[i], "needPassword" : true});
                        }

                        return jsonCreateByStr('getKeysList', true, res);
                    }

                    return jsonCreateErrorStr('getKeysList', 4, 'Данный метод недоступен в текущем состоянии сессии.');
                }

                function selectKey(params) {
                    if (params.alias == 'undefined' || params.password == 'undefined') {
                        return jsonCreateErrorStr('selectKey', 1, 'Команда сформирована некорректно.');
                    }

                    if (gateConfig.sessionState == 'selectFileStorage' || gateConfig.sessionState == 'selectKey') {
                        gateConfig.storage.selectKey(params.alias, params.password);
                        if (gateConfig.storage.errorCode != 0) {
                            return jsonCreateErrorByCode('selectKey', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser);
                        }
                        gateConfig.keyAlias = params.alias;
                        gateConfig.sessionState = 'selectKey';

                        return jsonCreateByStrWithoutSpecStr('selectKey', true);
                    }

                    return jsonCreateErrorStr('selectKey', 4, 'Данный метод недоступен в текущем состоянии сессии.');
                }

                function generateKeyPair(params) {
                    //TODO:
                    if (params.alias == 'undefined' || params.password == 'undefined' || params.signParams == 'undefined' || params.keyInfo == 'undefined') {
                        return jsonCreateErrorStr('generateKeyPair', 1, 'Команда сформирована некорректно.');
                    }

                    if (gateConfig.sessionState == 'selectFileStorage' || gateConfig.sessionState == 'selectKey') {
                        if (params.signParams == '') {
                            gateConfig.storage.generateKey(AIDS.DSTU4145.EC_M257_PB_SBOX1);
                        } else {
                            gateConfig.storage.generateKey(params.signParams.base64ToHex());
                        }
                        if (gateConfig.storage.errorCode != 0) {
                            return jsonCreateErrorByCode('generateKeyPair', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser);
                        }
                        gateConfig.storage.storeKey(params.alias, params.password);
                        if (gateConfig.storage.errorCode != 0) {
                            return jsonCreateErrorByCode('generateKeyPair', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser);
                        }
                        gateConfig.storage.selectKey(params.alias, params.password);
                        if (gateConfig.storage.errorCode != 0) {
                            return jsonCreateErrorByCode('generateKeyPair', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser);
                        }

                        gateConfig.keyAlias = params.alias;
                        return jsonCreateByStrWithoutSpecStr('generateKeyPair', true);
                    }

                    return jsonCreateErrorStr('generateKeyPair', 4, 'Данный метод недоступен в текущем состоянии сессии.');
                }

                function  getCertificate(params) {
                    //TODO: key usage
                    if (typeof params.keyUsage == 'undefined' && typeof params.keyUsage == 'undefined') {
                        return jsonCreateErrorStr('getCertificate', 1, 'Команда сформирована некорректно.');
                    }
                    
                    if (gateConfig.sessionState == 'selectKey') {
                        var keyUsage = 0;
                        for (var i = 0; i < params.keyUsage.length; i++) {
                            if (params.keyUsage[i] == 'digitalSignature') {
                                keyUsage |= 1 << 0;
                            } else if (params.keyUsage[i] == 'nonRepudiation') {
                                keyUsage |= 1 << 1;
                            } else if (params.keyUsage[i] == 'keyEncipherment') {
                                keyUsage |= 1 << 2;
                            } else if (params.keyUsage[i] == 'dataEncipherment') {
                                keyUsage |= 1 << 3;
                            } else if (params.keyUsage[i] == 'keyAgreement') {
                                keyUsage |= 1 << 4;
                            } else if (params.keyUsage[i] == 'keyCertSign') {
                                keyUsage |= 1 << 5;
                            } else if (params.keyUsage[i] == 'crlSign') {
                                keyUsage |= 1 << 6;
                            } else if (params.keyUsage[i] == 'encipherOnly') {
                                keyUsage |= 1 << 7;
                            } else if (params.keyUsage[i] == 'decipherOnly') {
                                keyUsage |= 1 << 8;
                            } else {

                            }
                        }
                        var cert = gateConfig.storage.getCertificate(keyUsage);
                        if (gateConfig.storage.errorCode != 0) {
                            return jsonCreateErrorByCode('getCertificate', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser);
                        }
                        return jsonCreateByStr('getCertificate', true, {"certificate": cert});
                    }
                    return jsonCreateErrorStr('getCertificate', 4, 'Данный метод недоступен в текущем состоянии сессии.');
                }

                function getCertificateChain(params) {
                    //TODO: key usage
                    if (typeof params.alias == 'undefined') {
                        return jsonCreateErrorStr('getCertificateChain', 1, 'Команда сформирована некорректно.');
                    }
                    var currKeyAlias = gateConfig.keyAlias;
                    if (gateConfig.sessionState === 'selectFileStorage' || gateConfig.sessionState === 'selectKey') {

                        gateConfig.storage.selectKey(params.alias, '');

                        if (gateConfig.storage.errorCode != 0) {
                            return jsonCreateErrorByCode('getCertificateChain', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser);
                        }
                        var certs = gateConfig.storage.getCertificates();
                        if (gateConfig.storage.errorCode != 0) {
                            if (currKeyAlias != '') {
                                gateConfig.storage.selectKey(currKeyAlias, '');
                            }
                            return jsonCreateErrorByCode('getCertificateChain', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser);
                        }
                        if (currKeyAlias != '') {
                            gateConfig.storage.selectKey(currKeyAlias, '');
                        }
                        if (gateConfig.storage.errorCode != 0) {
                            return jsonCreateErrorByCode('getCertificateChain', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser);
                        }
                        return jsonCreateByStr('getCertificateChain', true, {"certificate": certs});
                    }
                    if (currKeyAlias != '') {
                        gateConfig.storage.selectKey(currKeyAlias, '');
                    }
                    if (gateConfig.storage.errorCode != 0) {
                        return jsonCreateErrorByCode('getCertificateChain', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser);
                    }
                    return jsonCreateErrorStr('getCertificateChain', 4, 'Данный метод недоступен в текущем состоянии сессии.');
                }

                function setCertificateChain(params) {
                    if (params.certificateChain == 'undefined') {
                        return jsonCreateErrorStr('setCertificateChain', 1, 'Команда сформирована некорректно.');
                    }
                    if (gateConfig.sessionState == 'selectKey') {
                        for (var i = 0; i < params.certificateChain.length; i++) {
                            gateConfig.storage.setCertificates(params.certificateChain[i]);
                            if (gateConfig.storage.errorCode != 0) {
                                return jsonCreateErrorByCode('setCertificateChain', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser);
                            }
                        }
                        return jsonCreateByStr('setCertificateChain', true, '');
                    }
                    return jsonCreateErrorStr('setCertificateChain', 4, 'Данный метод недоступен в текущем состоянии сессии.');
                }

                function getCertificateReq(params) {
                    if (params.subject == 'undefined' || params.attributes == 'undefined') {
                        return jsonCreateErrorStr('getCertificateReq', 1, 'Команда сформирована некорректно.');
                    }

                    if (gateConfig.sessionState == 'selectKey') {
                        var subjStr = '';
                        var attrStr = '';
                        for (var i = 0; i < params.subject.length; i++) {
                            subjStr += '{' + params.subject[i] + '},';
                        }
                        for (var i = 0; i < params.attributes.length; i++) {
                            attrStr += '{' + params.attributes[i] + '},';
                        }

                        var certReq = gateConfig.storage.generateCertificateRequest(subjStr, '', '', attrStr);

                        if (gateConfig.storage.errorCode != 0) {
                            return jsonCreateErrorByCode('getCertificateReq', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser);
                        }
                        return jsonCreateByStr('getCertificateReq', true, [certReq]);
                    }

                    return jsonCreateErrorStr('getCertificateReq', 4, 'Данный метод недоступен в текущем состоянии сессии.');
                }

                function signData(params) {
                    if (params.data == 'undefined' || params.hashParams == 'undefined') {
                        return jsonCreateErrorStr('signData', 1, 'Команда сформирована некорректно.');
                    }

                    if (gateConfig.sessionState == 'selectKey') {
                        gateConfig.storage.setSignHash(params.hashParams);
                        if (gateConfig.storage.errorCode != 0) {
                            return jsonCreateErrorByCode('signData', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser);
                        }

                        return jsonCreateByStr('signData', true, {"sign": gateConfig.storage.signData(params.data)});
                    }

                    return jsonCreateErrorStr('signData', 4, 'Данный метод недоступен в текущем состоянии сессии.');
                }

                function signHash(params) {
                    if (typeof params.hashData == 'undefined') {
                        return jsonCreateErrorStr('signHash', 1, 'Команда сформирована некорректно.');
                    }

                    if (gateConfig.sessionState == 'selectKey') {
                        var res = gateConfig.storage.signHash(params.hashData);
                        if (gateConfig.storage.errorCode != 0) {
                            return jsonCreateErrorByCode('signHash', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser);
                        }
                        return jsonCreateByStr('signHash', true, {"sign": res});

                    }

                    return jsonCreateErrorStr('signHash', 4, 'Данный метод недоступен в текущем состоянии сессии.');
                }

                function CMSSign(params) {
                    if (typeof params.data == 'undefined' || typeof params.hashParams == 'undefined' || typeof params.certificate == 'undefined' ||
                        typeof params.timeStamp == 'undefined') {
                        return jsonCreateErrorStr('CMSSign', 1, 'Команда сформирована некорректно.');
                    }

                    var includeData = false;
                    var includeCertificate = false;
                    var tspCertReq = false;
                    var certificate = '';
                    //Проверяем наличие флагов, если они отсутсвуют, берем false
                    if (typeof params.includeData != 'undefined') {
                        includeData = params.includeData;
                    }
                    if (typeof params.includeCertificate != 'undefined') {
                        includeCertificate = params.includeCertificate;
                    }
                    if (typeof params.includeCertificate != 'undefined') {
                        tspCertReq = params.TSPcertReq;
                    }

                    //Проверка на состояние храналища
                    if (gateConfig.sessionState !== 'selectKey') {
                        return jsonCreateErrorStr('CMSSign', 4, 'Данный метод недоступен в текущем состоянии сессии.');
                    }
                    if (params.hashParams != '') {
                        gateConfig.storage.setSignHash(params.hashParams.base64ToHex());
                    } else {
                        //gateConfig.storage.setSignHash(AIDS.HASH.GOST34311);
                    }
                    if (gateConfig.storage.errorCode != 0) {
                        return jsonCreateErrorByCode('CMSSign', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser);
                    }

                    var tspresp = '';
                    //Проверка на пристутсвие tsp
                    if (params.timeStamp != '') {
                        tspresp = params.timeStamp;
                    }

                    if (params.certificate == '') {
                        certificate = gateConfig.storage.getCertificate(1);
                    } else {
                        certificate = params.certificate
                    }
                    if (certificate == '') {
                        return jsonCreateErrorStr('CMSSign', 107, 'Не найден сертификат.');
                    }

                    var res = gateConfig.storage.signCMS(certificate, params.data, includeData, includeCertificate, tspresp, '');

                    if (gateConfig.storage.errorCode != 0) {
                        return jsonCreateErrorByCode('CMSSign', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser);
                    }

                    return jsonCreateByStr('CMSSign', true, {"sign": res});
                }

                function CMSSignHash(params) {
                    if (typeof params.hashData == 'undefined' || typeof params.hashParams == 'undefined' || typeof params.certificate == 'undefined' ||
                        typeof params.timeStamp == 'undefined') {
                        return jsonCreateErrorStr('CMSSign', 1, 'Команда сформирована некорректно.');
                    }

                    var includeData = false;
                    var includeCertificate = false;
                    var tspCertReq = false;
                    var certificate = '';
                    //Проверяем наличие флагов, если они отсутсвуют, берем false
                    if (typeof params.includeData != 'undefined') {
                        includeData = params.includeData;
                    }
                    if (typeof params.includeCertificate != 'undefined') {
                        includeCertificate = params.includeCertificate;
                    }
                    if (typeof params.includeCertificate != 'undefined') {
                        tspCertReq = params.TSPcertReq;
                    }

                    //Проверка на состояние храналища
                    if (gateConfig.sessionState !== 'selectKey') {
                        return jsonCreateErrorStr('CMSSign', 4, 'Данный метод недоступен в текущем состоянии сессии.');
                    }
                    if (params.hashParams != '') {
                        gateConfig.storage.setSignHash(params.hashParams.base64ToHex());
                    } else {
                        //gateConfig.storage.setSignHash(AIDS.HASH.GOST34311);
                    }
                    if (gateConfig.storage.errorCode != 0) {
                        return jsonCreateErrorByCode('CMSSign', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser);
                    }

                    var tspresp = '';
                    //Проверка на пристутсвие tsp
                    if (params.timeStamp != '') {
                        tspresp = params.timeStamp;
                    }

                    if (params.certificate == '') {
                        certificate = gateConfig.storage.getCertificates()[0];
                    } else {
                        certificate = params.certificate
                    }
                    if (certificate == '') {
                        return jsonCreateByStr('CMSSign', false, 'CMS sign failed. Certificate wasn\'t found');
                    }

                    var res = gateConfig.storage.signHashCMS(certificate, params.hashData, includeData, includeCertificate, tspresp, '');

                    if (gateConfig.storage.errorCode != 0) {
                        return jsonCreateErrorByCode('CMSSign', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser);
                    }

                    return jsonCreateByStr('CMSSign', true, {"sign": res});
                }

                function getTimeStampReq(params) {
                    if (typeof params.data == 'undefined' || typeof params.hashParams == 'undefined' ||
                        typeof params.reqPolicy == 'undefined' || typeof params.nonce == 'undefined') {
                        return jsonCreateErrorStr('getTimeStampReq', 1, 'Команда сформирована некорректно.');
                    }
                    var certReq = false;
                    if (gateConfig.sessionState === 'selectKey') {
                        if (typeof params.certReq != 'undefined') {
                            certReq = params.certReq;
                        }
                        var hashParams = '';
                        var reqPolicy = '';
                        var signingTime = false;
                        if (params.hashParams == '') {
                            hashParams = AIDS.HASH.GOST34311.hexToBase64();
                        } else {
                            hashParams = params.hashParams;
                        }
                        if (params.reqPolicy == '') {
                            reqPolicy = '1.2.804.2.1.1.1.2.3.1';
                        } else {
                            reqPolicy = params.reqPolicy;
                        }
                        if (params.signingTime != '' && params.signingTime === true) {
                            signingTime = true;
                        }

                        var ret = gateConfig.storage.generateTspReq(params.data, hashParams, reqPolicy, params.nonce, certReq);
                        return jsonCreateByStr('getTimeStampReq', true, {"timeStampReq": ret});
                    }

                    return jsonCreateErrorStr('getTimeStampReq', 4, 'Данный метод недоступен в текущем состоянии сессии.');
                }

                function getCertificateInfo(params) {
                    if (typeof params.certificate === 'undefined' || params.certificate == '') {
                        return jsonCreateErrorStr('getCertificateInfo', 1, 'Команда сформирована некорректно.');
                    }

                    var res = gateConfig.storage.getCertificateInfo(params.certificate);
                    if (gateConfig.storage.errorCode != 0) {
                        return jsonCreateErrorStr('getCertificateInfo', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser, 'Get certificate info failed.');
                    }

                    return jsonCreateByStr('getCertificateInfo', true, res);
                }

                function getOCSPReq(params) {
                    if (typeof params.certificate === 'undefined' || typeof params.rootCertificate === 'undefined' || params.certificate === '' || params.rootCertificate === '') {
                        return jsonCreateErrorStr('getOCSPReq', 1, 'Команда сформирована некорректно.');
                    }

                    var res = gateConfig.storage.getOCSPReq(params.rootCertificate, params.certificate);
                    if (gateConfig.storage.errorCode != 0) {
                        return jsonCreateErrorStr('getCertificateInfo', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser, 'Get OCSP request failed.');
                    }

                    return jsonCreateByStr('getOCSPReq', true, {"OCSPReq": res});
                }

                function CMSSplit(params) {
                    if (typeof params.CMS === 'undefined' || params.CMS == '') {
                        return jsonCreateErrorStr('CMSSplit', 1, 'Команда сформирована некорректно.');
                    }

                    var res = gateConfig.storage.CMSSplit(params.CMS);
                    if (gateConfig.storage.errorCode != 0) {
                        return jsonCreateErrorStr('CMSSplit', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser, 'CMS split failed.');
                    }

                    return jsonCreateByStr('CMSSplit', true, {"CMSArray": res});
                }

                function selectFile(params, success, fail){

                    return (function() {
                        var previousDiv = document.getElementById("fileDialogBox");
                        if (previousDiv) {
                            document.body.removeChild(previousDiv);
                        }
                        var fileInput = document.createElement('input');
                        fileInput.id = 'fileDialogBox';
                        fileInput.className = 'fileInput';
                        fileInput.setAttribute('type', 'file');
                        fileInput.style.visibility = 'hidden';
                        fileInput.addEventListener("change", function () {
                            var reader = new FileReader();
                            var fileInput = document.getElementById("fileDialogBox");
                            reader.addEventListener("load", function () {
                                var el = document.getElementById("fileDialogBox");
                                var name = el.value;
                                el.parentNode.removeChild(el);
                                success({"filePath": reader.result.split(',')[1], "name": name});
                            }, false);
                            reader.readAsDataURL(fileInput.files[0]);
                            if (reader.error !== null) {
                                fail({"errorCode": 111, "errorText": 'Файл не выбран.'});
                            }
                        });
                        document.body.appendChild(fileInput);
                        document.getElementById("fileDialogBox").click();
                    })();
                }

                function CMSJoin(params) {
                    if (typeof params.CMSArray === 'undefined') {
                        return jsonCreateErrorStr('CMSJoin', 1, 'Команда сформирована некорректно.');
                    }

                    if (params.CMSArray.length == 1) {
                        return jsonCreateByStr('CMSJoin', true, {"CMS": params.CMSArray[0]});
                    }

                    var res = gateConfig.storage.CMSJoin(null, params.CMSArray[0], params.CMSArray[1]);
                    if (gateConfig.storage.errorCode != 0) {
                        return jsonCreateErrorStr('CMSJoin', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser, 'CMS Join failed.');
                    }
                    for (var i = 2; i < params.CMSArray.length; i++) {
                        res = gateConfig.storage.CMSJoin(null, res, params.CMSArray[i]);
                        if (gateConfig.storage.errorCode != 0) {
                            return jsonCreateErrorStr('CMSJoin', gateConfig.storage.errorCode + cryptoniteXErrorIncreaser, 'CMS join failed.');
                        }
                    }

                    return jsonCreateByStr('CMSJoin', true, {"CMS": res});
                }

                return instance;
            }
        })()
    };
// return window.CryptoPluginJS;
}