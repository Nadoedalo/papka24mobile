all: build

build: js_build

release : js_release

js_build: js_clean
		@echo 'Building app now'
		@npm run build
		@echo 'Build finished'

js_release:
		@echo 'Releasing app now'
		@npm run release
		@npm run release_move
		@echo 'Release finished'

js_clean:
		@echo 'JS clean started'
		@rm -rf ./ver_*
		@echo 'JS clean finished'
js_full_clean: js_clean
		@echo 'JS full clean started'
		@rm -rf ./node_modules
		@rm -rf ./build
		@echo 'JS full clean finished'