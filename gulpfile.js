/*
 * TODO feature list:
 * git-version dest folder
 * change index.html links to new version
 * uglyfy/minify/etc to files
 * sass && auto-sprite all images
 * */
var gulp = require('gulp'),
    gulpif = require('gulp-if'),
    concat = require('gulp-concat'),
    exec = require('child_process').exec,
    spawn = require('child_process').spawn,
    replace = require('gulp-replace'),
    replacer = new RegExp('{{%VERSION%}}', 'g'),
    version = 'ver_no_version',
    uglify = require('gulp-uglify'),
    stripJsonComments = require('gulp-strip-json-comments'),
    release = '', //TODO fixme
    releasePath = '/var/www/papka-m/';
gulp.task('clean', function(){
    return spawn('rm', ['-rf', './build']);
});
gulp.task('clean_release', function(){
    return spawn('rm', ['-rf', releasePath+'*']);
});

gulp.task('build', ['version'], function () {
    console.log('Current version: '+version);
    release = 'build/';
    gulp.start('build_true'); // TODO fixme похоже на костыль
});
gulp.task('release_move', ['clean_release'], function(){
    gulp.src('./build/**/*')
        .pipe(gulp.dest(releasePath));
});
gulp.task('release', ['clean', 'version'], function(){
    console.log('Current version: '+version);
    release = 'build/';
    gulp.start('build_true'); // TODO fixme похоже на костыль

});
gulp.task('build_lib', ['clean_release'], function(){
    gulp.src('./lib/*.js')
        .pipe(uglify())
        .pipe(replace(replacer, version))
        .pipe(gulp.dest('./build/lib/'));
});
gulp.task('build_true', ['build_js', 'build_html', 'build_lib', 'build_css', 'build_fonts', 'build_img', 'build_index_html', 'build_language']);

gulp.task('version', ['clean'], function(){
    var git = spawn('git', ['describe', '--long']);
    return git.stdout.on('data', function(data){
        data = data.toString('utf8').replace(/\s/g, '');
        exec('echo '+data+' >  ./version.txt');
        version = 'ver_'+data;                    // TODO: FIX ME
    });
});
/*
 * TODO сборка всех js-файликов в kernel.js с поддержкой require
 * */
gulp.task('build_js', ['version'], function () {
    console.log(version, 'Running bild_js');
    gulp.src(
        [
            './source/js/**/*/*',
            './source/js/*.*'
        ])
    //.pipe(uglify())
        .pipe(replace(replacer, version))
        .pipe(gulp.dest('./build/' + version + '/js'));
});
/*
 * TODO слепливание css-файлов в один базовый css
 * TODO sass, compass, image minify && etc
 * */

gulp.task('build_css', function(){

    gulp.src('./source/design/css/*.css')
        .pipe(replace(replacer, version))
        .pipe(gulp.dest('./'+release+version+'/design/css'));

});
/*
 * just placing fonts in right place
 * */
gulp.task('build_fonts', function(){
    gulp.src('./source/design/fonts/*')
        .pipe(gulp.dest('./'+release+version+'/design/fonts'));
});
gulp.task('build_html', function(){
    gulp.src('./source/html/templates/*')
        .pipe(replace(replacer, version))
        .pipe(gulp.dest('./'+release+version+'/html/'));
});
gulp.task('build_language', function(){
    /*
     * TODO extend language object with each other
     * */
    gulp.src('./source/html/languages/**/**/*.json', {base : './source/html/languages/'})
        .pipe(stripJsonComments())
        .pipe(gulp.dest('./'+release+version+'/html/languages/'));
});
gulp.task('build_img', function(){
    gulp.src('./source/design/img/*')
        .pipe(gulp.dest('./'+release+version+'/design/img'));
});
gulp.task('build_index_html', function(){
    gulp.src([
        './source/index.html'
    ])
        .pipe(replace(replacer, version))
        .pipe(gulp.dest('./'+release))
        .pipe(gulp.dest('./'+release+'/'+version));
});