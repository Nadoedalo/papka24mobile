/*jslint nomen: true, unparam: true*/
require([
    "nativeBackbone",
    "underscore",
    "backbone",
    "js/app",
    "js/router"
], function (nativeBackbone, _, Backbone, App, Router) {
    "use strict";
    /**
     * Draws preloader on a specific parent OR on body
     * */
    App.drawPreloader = function (el) {
        if (!el) {
            el = window.document.body;
        }
        var preloader = window.document.createElement('DIV'),
            img = window.document.createElement('IMG');
        preloader.className = 'preloader';
        img.setAttribute('src', '/{{%VERSION%}}/design/img/preloader.gif');
        preloader.appendChild(img);
        el.appendChild(preloader);
    };
    /**
     * removes all preloaders it finds on a specific parent OR on body
     * */
    App.removePreloader = function (el) {
        if (!el) {
            el = window.document.body;
        }
        var temp = el.querySelectorAll('.preloader');
        for(var i = 0, length = temp.length; i < length; i++){
            temp[i].parentNode.removeChild(temp[i]);
        }
    };
    App.router = new Router;
    Backbone.history.start({ pushState: true, root: App.root });

    /*making menu work*/
    function toggleClassMenu() {
        var myMenu = document.querySelector('.menu');
        myMenu.classList.add('menu--animatable');
        myMenu.classList.toggle('menu--visible');
    }

    function OnTransitionEnd() {
        var myMenu = document.querySelector('.menu');
        myMenu.classList.remove('menu--animatable');
    }
    var ElementProto = (typeof Element !== 'undefined' && Element.prototype) || {};
    // Find the right `Element#matches` for IE>=9 and modern browsers.
    var matchesSelector = ElementProto.matches ||
        ElementProto.webkitMatchesSelector ||
        ElementProto.mozMatchesSelector ||
        ElementProto.msMatchesSelector ||
        ElementProto.oMatchesSelector ||
        // Make our own `Element#matches` for IE8
        function(selector) {
            // Use querySelectorAll to find all elements matching the selector,
            // then check if the given element is included in that list.
            // Executing the query on the parentNode reduces the resulting nodeList,
            // (document doesn't have a parentNode).
            var nodeList = (this.parentNode || document).querySelectorAll(selector) || [];
            return ~indexOf(nodeList, this);
        };
    document.body.addEventListener('click', function(e){
        var selector = '.app-menu';
        var node = e.target || e.srcElement;
        for (; node && node !== document.body; node = node.parentNode) {
            if (matchesSelector.call(node, selector)) {
                e.delegateTarget = node;
                e.stopPropagation();
            }
        }
    });
    document.body.addEventListener('transitionend', function(e){
        var selector = '.menu';
        var node = e.target || e.srcElement;
        for (; node && node !== document.body; node = node.parentNode) {
            if (matchesSelector.call(node, selector)) {
                e.delegateTarget = node;
                OnTransitionEnd();
            }
        }
    });
    document.body.addEventListener('click', function(e){
        var selector = '.menu-icon, .menu';
        var node = e.target || e.srcElement;
        for (; node && node !== document.body; node = node.parentNode) {
            if (matchesSelector.call(node, selector)) {
                e.delegateTarget = node;
                toggleClassMenu();
            }
        }
    });
});