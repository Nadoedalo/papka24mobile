/*jslint nomen:true, todo: true, regexp: true, unparam: true*/
define([
    'underscore',
    'backbone',
    'exports',
    'js/modules/User/UserModel',
    'js/modules/DocumentsCollection/DocumentsCollectionModel',
    'js/modules/Document/DocumentModel',
    'js/modules/DocumentsCollection/DocumentsCollection',
    'templateHelper'
], function (_,
             Backbone,
             exports,
             UserModel,
             DocumentsCollectionModel,
             DocumentModel,
             DocumentsCollection
) {
    "use strict";
    var _sync = Backbone.sync;
    Backbone.sync = function(method, model, options){
        options.beforeSend = function(xhr){//FIXME может ли пригодится beforeSend?
            xhr.setRequestHeader('sessionId', userModel.get('sessionId'));
            xhr.setRequestHeader('v', '170117'); /*fixme use gradle for version num*/
        };
        return _sync.call(this, method, model,options);
    };
    var userModel = new UserModel(),
        documentsCollection = new DocumentsCollection([], {model : DocumentsCollectionModel});
    // The root path to run the application through.
    exports.root = "";
    exports.userModel = userModel;
    exports.DocumentModel = DocumentModel;
    exports.documentsCollection = documentsCollection;
});

