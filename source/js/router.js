/*jslint nomen: true, unparam: true, todo: true*/
define([
    'underscore',
    'backbone',
    'js/app',
    'require'
], function (_, Backbone, App, require) {
    "use strict";
    return Backbone.Router.extend({
        routes: {
            '': 'authCheck',
            'login' : 'login',
            'docs' : 'documentList',
            'doc/:id' : 'documentView',
            'docs/page/:pageNum' : 'documentListPage',
            'docs/tag/:tagId' : 'documentListTag',
            'docs/tag/:tagId/page/:pageNum' : 'documentListTagPage',
            'trash' : 'trashList',
            'trash/page/:pageNum' : 'trashList',
            'error' : 'login', //fixme actual error page?
            '*notFound': 'notFound'
        },
        initialize: function () {
/*            this.on('route', this.onRoute);*/
            window.document.addEventListener('click', function(event){
                var target = event.target,
                    href = target.getAttribute('href');
                if (target.hostname === window.location.hostname && target.getAttribute('target') !== '_blank' && href && href !== '#') {
                    event.preventDefault();
                    this.navigate(href, {trigger: true});
                }
            }.bind(this));
            require([
                'js/modules/Title/TitleView',
                'js/modules/UserProfile/UserProfile',
                'js/modules/Navigation/Navigation'
            ], function(TitleView, UserProfile, Navigation){
                var titleView = new TitleView(),
                    userProfile = new UserProfile({model : App.userModel}),
                    navigation = new Navigation(),
                    titleViewNode = document.querySelector('.titleView'),
                    userProfileNode = document.querySelector('.userProfile'),
                    navigationNode = document.querySelector('.navigation');
                titleViewNode.parentNode.replaceChild(titleView.el, titleViewNode);
                userProfileNode.parentNode.replaceChild(userProfile.el, userProfileNode);
                navigationNode.parentNode.replaceChild(navigation.el, navigationNode);
            });
        },
        onRoute: function () {
            // gathering trash
            //TODO no trash to gather now?
        },
        execute : function(callback, args) { // preventing navigation if not authorized
            var node = document.querySelector('.main');
            while (node.firstChild) { //fixme?
                node.removeChild(node.firstChild);
            }
            if(callback && !App.userModel.requireAuth(this.current().route)){
                callback.apply(this, args);
            }else{
                var auth = App.userModel.checkAuthorization();
                if(typeof auth === 'object') {
                    auth
                        .then(
                            function(){ //done
                                this.execute.call(this, callback, args);
                            }.bind(this),
                            function(){ //fail
                                this.navigate('login', {trigger: true});
                            }.bind(this)
                        )
                    return false;
                }
                if (callback && auth){
                    document.querySelector('.header').classList.remove('hidden');
                    callback.apply(this, args);
                } else {
                    this.navigate('login', {trigger: true});
                    return false;
                }
            }
        },
        current : function() {
            var Router = this,
                fragment = Backbone.history.fragment,
                routes = _.pairs(Router.routes),
                route = null, params = null, matched, callback = null;
            matched = _.find(routes, function(handler) {
                route = _.isRegExp(handler[0]) ? handler[0] : Router._routeToRegExp(handler[0]);
                return route.test(fragment);
            });
            if(matched) {
                params = Router._extractParameters(route, fragment);
                route = matched[0];
                callback = matched[1];

            }
            return {
                route : route,
                fragment : fragment,
                params : params,
                callback : callback
            };
        },
        authCheck : function(){
            var auth = App.userModel.checkAuthorization();
            if(typeof auth === 'object'){
                auth
                    .then(
                        function(){//done
                            this.navigate('/docs', {trigger : true});
                        }.bind(this),
                        function(){//fail
                            this.navigate('/login', {trigger : true});
                        }.bind(this)
                    );
            }else if(auth){
                this.navigate('/docs', {trigger : true});
            }else{
                this.navigate('/login', {trigger : true});
            }

        },
        login : function(){
            document.querySelector('.header').classList.add('hidden');
            require([
                'js/modules/Login/LoginView'
            ], function(LoginView){
                var model = App.userModel,
                    view = new LoginView({model: model}),
                    node = document.querySelector('.main');
                while (node.firstChild) {
                    node.removeChild(node.firstChild);
                }
                node.appendChild(view.el);
            });
        },
        documentList : function(page, tag){
            require([
                'js/modules/DocumentList/DocumentListModel',
                'js/modules/DocumentList/DocumentListCollection',
                'js/modules/DocumentList/DocumentListCollectionView'
            ], function(DocumentListModel, DocumentListCollection, DocumentListCollectionView){
                var collection = new DocumentListCollection(null, {
                        model : DocumentListModel,
                        page : page,
                        tag : tag
                    }),
                    view = new DocumentListCollectionView({collection: collection}),
                    node = document.querySelector('.main');
                while (node.firstChild) {
                    node.removeChild(node.firstChild);
                }
                node.appendChild(view.el);
            });
        },
        documentListPage : function(pageNum){
            this.documentList(pageNum);
        },
        documentListTag : function(tagId){
            this.documentList(null, tagId);
        },
        documentListTagPage : function(tagId, pageNum){
            this.documentList(pageNum, tagId);
        },
        trashList : function(page){
            require([
                'js/modules/TrashList/TrashListModel',
                'js/modules/TrashList/TrashListCollection',
                'js/modules/TrashList/TrashListCollectionView'
            ], function(TrashListModel, TrashListCollection, TrashListCollectionView){
                var collection = new TrashListCollection(null, {
                        model : TrashListModel,
                        page : page
                    }),
                    view = new TrashListCollectionView({collection: collection}),
                    node = document.querySelector('.main');
                while (node.firstChild) {
                    node.removeChild(node.firstChild);
                }
                node.appendChild(view.el);
            });
        },
        documentView : function(id){
            require([
                'js/modules/DocumentView/DocumentView'
            ], function(DocumentView){
                var model = App.documentsCollection.get(id) || new App.DocumentModel({id : id}),
                    view = new DocumentView({model: model}),
                    node = document.querySelector('.main');
                while (node.firstChild) {
                    node.removeChild(node.firstChild);
                }
                node.appendChild(view.el);
            });
        },
        notFound : function(){
            this.navigate('error', {trigger: true});
        }
    });
});