define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    "use strict";
    return Backbone.Collection.extend({
        url: '/api/resource/search',
        defaults : {
            author : "all",
            contractor : null,
            dateFrom : null,
            dateTo : null,
            docList : "docs",
            limit : 25,
            offset : 0,
            searchQuery : "",
            tagFilter : null
        },
        attributes : {},
        setAttr : function(key, val){
            var attrs;
            if (typeof key === 'object') {
                attrs = key;
            } else {
                (attrs = {})[key] = val;
            }
            _.extend(this.attributes, attrs);
            return this;
        },
        getAttr : function(key){
            return this.attributes[key];
        },
        initialize: function (models, options) {
            //TODO parse localStorage
            //TODO webSocket support
            //TODO single channel for all browser tabs
            //TODO listening to doc status change
            //TODO collection of trash, docs etc
            _.extend(this.attributes, this.defaults);
            return this;
        },
        fetchCollection : function(attributes){
            //TODO should not always use HTTP, should return known models
            return this.fetch({
                data : JSON.stringify(attributes || this.attributes),
                type : 'POST'
            });
        }
    });
});