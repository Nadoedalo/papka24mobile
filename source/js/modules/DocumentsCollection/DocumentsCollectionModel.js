define([
    'underscore',
    'backbone',
    'js/app'
], function (_, Backbone, App) {
    "use strict";
    return Backbone.Model.extend({
        urlRoot: '/api/resource',
        defaults : {
            hash : '',
            size : null,
            time : null,
            type : null,
            src : '',
            name : '',
            author : '',
            status : null,
            signs : null,
            tags : null,
            signed : false,
            shares : {},
            companyId : null,
            deleteByOwner : false
        },
        initialize: function () {
            if(this.get('author') != App.userModel.get('company').login){
                this.set('signed', this.get('shares')[App.userModel.get('company').login] % 10 === 3);
            }
            return this;
        },
        parse: function (data, options) {
            if(!data){return;}
            if (options.parseOption) {
                return this[options.parseOption](data);
            }
            return data;
        },
        deleteDoc : function(){
            return this.set('status', this.get('status') % 10 + 10);
        },
        restoreDoc : function(){
            return this.set('status', this.get('status') % 10);
        },
        tagDoc : function(){
            //TODO хитрая схема тегирования
        },
        signDoc : function(){
            //TODO в процессе разработки
        },
        addShares : function(shareArr, comment, mode){
            return this.save({}, {
                type : 'POST',
                url : '/api/share/'+this.id,
                data : JSON.stringify({
                    comment : comment || '',
                    mode : mode || 0,
                    emails : shareArr
                }),
                parseOption : 'parseAddedShares'
            });
        },
        parseAddedShares : function(resp){
            var id = this.id,
                obj = resp.reduce(function(memo, i){
                    if(i.resourceId === id){
                        memo[i.user] = i.status;
                    }
                    return memo;
                }, {});
            return {
                shares : obj
            };
        },
        fetchShares : function(){
            return this.fetch({
                url : '/api/share/'+this.id,
                parseOption : 'parseAddedShares'
            });
        },
        fetchSigns : function(){
            return this.fetch({
                url : '/api/sign'+this.id,
                parseOption : 'parseSign'
            });
        },
        parseSign : function(){
            //TODO parse sign
            return;
        },
        restore : function(){
            return this.save(null, {
                type : 'PUT',
                url : '/api/resource/restore/'+this.id,
                data : ''
            });
        }
    });
});