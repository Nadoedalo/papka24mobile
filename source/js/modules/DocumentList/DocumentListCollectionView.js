define([
    'underscore',
    'backbone',
    'js/modules/DocumentList/DocumentListView'
], function (_, Backbone, DocumentListView) {
    "use strict";
    return Backbone.NativeView.extend({
        tagName: 'div',
        className: 'documentListCollectionView',
        events: {
            'click .previous' : 'previous',
            'click .next' : 'next'
        },
        initialize: function () {
            this.collection.on('reset', this.render, this);
            this.collection.on('add', this.addOne, this);
            this.render();
            return this;
        },
        render: function () {
            this.template().then(function (html) {
                this.el.innerHTML = html;
                _.each(this.collection.models, this.addOne, this);
            }.bind(this));
            return this;
        },
        addOne : function(model){
            var view = new DocumentListView({
                model : model
            });
            this.el.querySelector('.documentListContent').appendChild(view.el);
            return this;
        },
        previous : function(){
            var offset = this.collection.getAttr('offset'),
                limit = this.collection.getAttr('limit'),
                newOffset = offset - limit;
            if(newOffset < 0){newOffset = 0;}
            this.collection.setAttr('offset', newOffset);
            this.collection.fetchCollection();
        },
        next : function(){
            var offset = this.collection.getAttr('offset'),
                limit = this.collection.getAttr('limit'),
                newOffset = offset + limit;
            this.collection.setAttr('offset', newOffset);
            this.collection.fetchCollection();
        }
    });
});