define([
    'underscore',
    'backbone',
    'js/modules/DocumentsCollection/DocumentsCollection',
    'js/app'
], function (_, Backbone, DocumentsCollection, App) {
    "use strict";
    return DocumentsCollection.extend({
        attributes : {
            docList : 'docs'
        },
        initialize: function (models, options) {
            //TODO parse localStorage
            this.attributes = _.extend(this.defaults, this.attributes);
            this.setAttr({
                tagFilter : options.tag || null,
                offset : (options.page || 0) * this.getAttr('limit')
            });
            this.fetchCollection();
            this.listenTo(App.documentsCollection, 'change', this.updateCollection);
            this.listenTo(App.router, 'route', function(){this.stopListening();});
            return this;
        },
        fetchCollection : function(){
            return App.documentsCollection.fetchCollection(this.attributes).then(function(resp){
                resp = resp.sort(function (a, b) {
                    if (a.time > b.time) {
                        return -1;
                    } else if (a.time < b.time) {
                        return 1;
                    } else {
                        return 0;
                    }
                });
                this.reset(resp);
                //this.add(resp);
            }.bind(this));
        },
        updateCollection : function(changedModel){
            if(this.get(changedModel.get('id'))){
                this.get(changedModel.get('id')).set(changedModel.attributes);
            }
        }
    });
});