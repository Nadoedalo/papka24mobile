define([
    'underscore',
    'backbone',
    'js/app',
    'js/modules/DocumentListShare/DocumentListShareModel',
    'js/modules/DocumentListShare/DocumentListShareView'
], function (_, Backbone, App, DocumentListShareModel, DocumentListShareView) {
    "use strict";
    return Backbone.NativeView.extend({
        tagName: 'div',
        className: 'documentListView',
        events: {
            'mousedown' : 'handleDown',
            'touchstart' : 'handleDown',
            'mouseup' : 'handleUp',
            'touchend' : 'handleUp'
        },
        timer : null,
        initialize: function () {
            this.model.on('change', this.render, this);
            this.model.on('destroy remove', this.remove, this);
            this.documentListShareModel = new DocumentListShareModel({}, {modelReference : this.model});
            this.documentListShareView =  new DocumentListShareView({model : this.documentListShareModel});
            this.render();
            return this;
        },
        render: function () {
            var data = _.clone(this.model.attributes);
            data.humanDate = this.toHumanReadable(data.time);
            data.shareCount = this.getShareCount();
            this.template(data, false, false).then(function (html) {
                this.el.innerHTML = html;
                var node = this.el.querySelector('.documentListShareView');
                node.parentNode.replaceChild(this.documentListShareView.el, node);
            }.bind(this));
            return this;
        },
        handleDown : function(e){
            if(!this.timer && (e.which === 1 || e.type === 'touchstart')){
                this.timer = window.setTimeout(function(){
                    this.selectDoc();
                    this.timer = null;
                }.bind(this), 100);
            }
        },
        handleUp : function(e){
            if(this.timer && (e.which === 1 || e.type === 'touchend')){
                window.clearTimeout(this.timer);
                this.timer = null;
                App.router.navigate('/doc/'+this.model.id, {trigger : true});
            }
        },
        selectDoc : function(){
            var element = this.el.querySelector('.checkboxOrIcon'),
                temp = element.innerText,
                str = '✔';
            console.log(temp);
            if(temp === str){
                str = element.getAttribute('shareCount');
                this.model.trigger('deSelected', this.model);
            }else{
                this.model.trigger('selected', this.model);
            }
            element.innerText = str;
            this.timer = null;
            this.el.classList.toggle('selected');
        },
        toHumanReadable : function(timestamp){
            var months = ['янв.', 'фев.', 'мар.', 'апр.', 'май', 'июнь', 'июль', 'авг.', 'сент.', 'нояб.', 'окт.', 'дек.'],
                date = new Date(timestamp);
            return date.getDate() + ' '+months[date.getMonth()];
        },
        getShareCount : function(){
            var count = Object.keys(this.model.get('shares')).length;
            if(count > 99){count = '99+';}
            return count;
        }

    });
});