define([
    'underscore',
    'backbone',
    'js/modules/DocumentList/DocumentListCollectionView',
    'js/modules/TrashList/TrashListView'
], function (_, Backbone, DocumentListCollectionView, TrashListView) {
    "use strict";
    return DocumentListCollectionView.extend({
        addOne : function(model){
            var view = new TrashListView({
                    model : model
                }),
                node = this.el.querySelector('.documentListContent');
            node.insertBefore(view.el, node.childNodes[0]);
            return this;
        }
    });
});