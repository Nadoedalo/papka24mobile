define([
    'underscore',
    'backbone',
    'js/modules/DocumentsCollection/DocumentsCollectionModel'
], function (_, Backbone, DocumentsCollectionModel) {
    "use strict";
    return DocumentsCollectionModel.extend({
    });
});