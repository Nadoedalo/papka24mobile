define([
    'underscore',
    'backbone',
    'js/modules/DocumentsCollection/DocumentsCollection',
    'js/app'
], function (_, Backbone, DocumentsCollection, App) {
    "use strict";
    return DocumentsCollection.extend({
        attributes : {
            docList : 'trash'
        },
        initialize: function (models, options) {
            //TODO parse localStorage
            this.attributes = _.extend(this.defaults, this.attributes);
            this.setAttr({
                tagFilter : options.tag || null,
                offset : (options.page || 0) * this.getAttr('limit')
            });
            this.fetchCollection();
            return this;
        },
        fetchCollection : function(){
            return App.documentsCollection.fetchCollection(this.attributes).then(function(resp){
                this.add(resp);
            }.bind(this));
        }
    });
});