define([
    'underscore',
    'backbone',
    'js/app'
], function (_, Backbone, App) {
    "use strict";
    return Backbone.NativeView.extend({
        tagName: 'div',
        className: 'documentListShareView',
        events: {},
        initialize: function () {
            this.render();
            this.model.on('change', this.render, this);
            return this;
        },
        render: function () {
            var data = _.clone(this.model.attributes);
            data.statusesImage = {
                0 : 'fa-question',
                1 : 'fa-eye-slash',
                2 : 'fa-eye',
                3 : 'fa-pencil',
                4 : 'fa-pencil',
                999999998 : 'fa-paw',
                999999999 : 'fa-paw'
            };
            data.emptyCheck = !Object.keys(this.model.get('shares')).length;
            this.template(data, this.className, false).then(function (html) {
                this.el.innerHTML = html;
            }.bind(this));
            return this;
        }
    });
});