define([
    'underscore',
    'backbone',
    'js/app'
], function (_, Backbone, App) {
    "use strict";
    return Backbone.Model.extend({
        urlRoot: '/api/share',
        defaults : {
            id : null,
            shares : {},
            deletedByOwner : false,
            friends : {}
        },
        initialize: function (attr, options) {
            this.modelReference = options.modelReference;
            this.updateFromRefModel(this.modelReference);
            this.updateFriends(App.userModel);
            this.listenTo(this.modelReference, 'change', this.updateFromRefModel);
            this.listenTo(App.userModel, 'change:friends', this.updateFriends);
            this.listenTo(App.router, 'route', function(){this.stopListening();});
            return this;
        },
        updateFromRefModel : function(changedModel){
            var tempShares = _.clone(changedModel.get('shares'));
            if(changedModel.get('author') != App.userModel.get('login')){
                delete tempShares[App.userModel.get('login')];
                tempShares[changedModel.get('author')] = changedModel.get('deleteByOwner') ? '999999998' : '999999999';
                this.set('shares', tempShares);
            }
            this.set({
                id : changedModel.get('id'),
                shares : tempShares,
                deleteByOwner : changedModel.get('deleteByOwner') || false,
                docAuthorCheck : changedModel.get('author') != App.userModel.get('login')
            });
        },
        updateFriends : function(changedModel){
            this.set({
                friends: changedModel.get('friends')
            });
        }
    });
});