define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    "use strict";
    return Backbone.Model.extend({
        url : '/api/login',
        idAttribute : 'login',
        userAuthorizationRequest : false,
        defaults: {
            userAuthorizationFlag: null,// true - has session, false - has not, null - not checked
            userSettings: {}, // TODO defaults listed here?
            login : '', //user login - email
            fullName : '', // user set name
            newPartner : 0, //FIXME хер пойми что это
            security : '', //FIXME хер пойми что это, видимо какие-то права
            sessionId : '', //собственно сессия, должна писаться к любому хедеру на ajax-запрос
            company : { //собственно, описание компании
                companyId : undefined, //number
                egrpou : null,
                /**
                 * employee : [{
                 *  companyId : number,
                 *  initiator : null || email, //who invited
                 *  login : email,
                 *  removeInitialor : null || email,
                 *  role : number //0 - admin, 1 - user
                 *  startDate : timestamp,
                 *  status : number, //0 - invited, 1 - accepted, 2 - rejected, 3 - fired
                 *  stopDate : 0 || timestamp // 0 - wasn't stopped
                 * }]
                 * */
                employee : [],
                name : ''
            },
            description : { //фигня какая-то, почему бы это всё в аттрибуты не запихнуть?
                config : null, //FIXME хер пойми что это
                filter : null, //FIXME хер пойми что это
                showWizard : false, //Показывать пошаговую инструкцию или нет
                showCloud : false, //FIXME хер пойми что это
                keyPath : null,
                tagList : [] //[{}, {}] where {} has id : number, text : string, color : number
            },//below are filled from description
            config : null,
            filter : null,
            showWizard : false,
            showCloud : false,
            keyPath : null,
            tagList : [],
            enableOTP : false,
            friends : {},// key - email, value - fullName
            /**
             * friendsCompany [{
             *  emails : [], //array of employees in that company with emails in it
             *  inn : number, //TODO switch key to egrpou
             *  name : String
             * }]
             * */
            friendsCompany : []
        },
        initialize: function () {
            try{ // expire mechanics?
                this.getFromLocalStorage();
            }catch(e){
                console.error('No LS support');
            }
            this.on('change', this.saveToLocalStorage, this);
            return this;
        },
        parse: function (data, options) {
            if(!data){return;}
            if (options.parseOption) {
                return this[options.parseOption](data);
            }
            return data;
        },
        auth : function(data){
            return this.save({},
                {
                    data : JSON.stringify(data),
                    parseOption : 'parseAuth',
                    validate : false,
                    type : 'POST'
                }
            );
        },
        checkAuthorization: function () {
            var userFlag = this.get('userAuthorizationFlag');
            var xhr = this.userAuthorizationRequest;
            if (userFlag === null && !xhr) {
                xhr = this.getAuthorization();
                this.userAuthorizationRequest = xhr;
                xhr.then(function(){return false;},function(){
                    this.set('userAuthorizationFlag', false);
                }.bind(this));
            }
            return _.isNull(userFlag) ? xhr : userFlag;
        },
        requireAuth: function (route) {
            var allowedUrls = ['login', 'error']; //white list
            return !(allowedUrls.indexOf(route) >= 0);
        },
        getAuthorization: function () {
            return this.fetch({
                parseOption: 'parseAuth'
            });
        },
        parseAuth: function (data) {
            data.description = JSON.parse(data.description);
            data.description.tagList = JSON.parse(data.description.tagList);
            data.friendsEmailsByCompany = (data.friendsCompany||[]).reduce(function(memo, i){
                var company = i.name;
                for(var y = 0, length = i.emails.length; y < length; y++){
                    if(!memo[i.emails[y]]){
                        memo[i.emails[y]] = company;
                    }else{
                        memo[i.emails[y]] += ', '+company;
                    }

                }
                return memo;
            }, {});
            data = _.extend(data, data.description, {userAuthorizationFlag : true});
            return data;
        },
        saveToLocalStorage : function(){
            //will save only auth flag for now TODO save more data to LS?
            window.localStorage.setItem('sessionId', this.get('sessionId'));
        },
        getFromLocalStorage : function(){
            var localStorageData = {
                sessionId : window.localStorage.getItem('sessionId')
            };
            this.set(localStorageData);
        },
        logout : function(){
            return this.destroy();
        },
        getFriendsCompanyByEmail : function(email){
            return this.get('friendsEmailsByCompany')[email] || '';
        }
    });
});