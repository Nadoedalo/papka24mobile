define([
    'underscore',
    'backbone',
    'js/modules/ShareList/ShareListCollection',
    'js/modules/ShareList/ShareListCollectionView',
    'js/modules/ShareList/ShareListModel',
    'js/modules/SignList/SignListCollection',
    'js/modules/SignList/SignListView',
    'js/modules/SignList/SignListModel',
    'js/app'
], function (_, Backbone, ShareListCollection, ShareListCollectionView, ShareListModel, SignListCollection, SignListView, SignListModel, App) {
    "use strict";
    return Backbone.NativeView.extend({
        tagName: 'div',
        className: 'documentView',
        events: {
            'click .back' : 'goBack',
            'click .documentTabs > div' : 'toggleTab',
            'click .delete' : 'deleteDocument',
            'click .restore' : 'restoreDocument'
        },
        initialize: function () {
            var tempShares = _.clone(this.model.get('shares')),
                shares;
            if(this.model.get('author') != App.userModel.get('login')){
                delete tempShares[App.userModel.get('login')];
                tempShares[this.model.get('author')] = this.model.get('deleteByOwner') ? '999999998' : '999999999';
            }
            shares = _.reduce(tempShares, function (memo, val, key) {
                memo.push({
                    email: key,
                    status: val
                });
                return memo;
            }, []);
            this.signListCollection = new SignListCollection([], {model : SignListModel, referenceModel: this.model});
            this.signListView = {};
            this.shareListCollection = new ShareListCollection(shares, {model : ShareListModel, referenceModel : this.model});
            this.shareListCollectionView = {};
            if(this.model.get('author')){//check if model has any data
                this.render();
            }else{
                this.drawPreloader();
            }
            this.model.on('change', this.render, this);
            return this;
        },
        render: function () {
            var data = _.clone(this.model.attributes);
            data.imageSrc = '/cdn/' + this.model.get('src')+'/'+this.model.get('hash')+'-50.png';
            data.pdfSrc = '/cdn/' + this.model.get('src')+'/'+this.model.get('hash');
            data.isAuthor = this.model.get('author') === App.userModel.get('login');
            this.template(data, '', false).then(function (html) {
                this.el.innerHTML = html;
                this.signListView = new SignListView({collection : this.signListCollection});
                this.shareListCollectionView = new ShareListCollectionView({collection : this.shareListCollection});
                this.shareListCollectionView.el.classList.add('hidden');
                this.signListView.el.classList.add('hidden');
                var node = this.el.querySelector('.shareListCollectionView');
                node.parentNode.replaceChild(this.shareListCollectionView.el, node);
                node = this.el.querySelector('.signList');
                node.parentNode.replaceChild(this.signListView.el, node);
            }.bind(this));
            return this;
        },
        toggleTab : function(e){
            var target = e.target;
            if(!target.getAttribute('data-tab')){
                return;
            }
            _.each(this.el.querySelectorAll('.documentTabs > div'), function(val){
                val.classList.remove('active');
            });
            target.classList.add('active');
            _.each(this.el.querySelectorAll('.documentContent > div'), function(val){
                val.classList.add('hidden');
            });
            this.el.querySelector('.documentContent > .'+target.getAttribute('data-tab')).classList.remove('hidden');
        },
        drawPreloader : function(){
            this.el.innerHTML = '<i class="fa fa-cog fa-spin fa-3x fa-fw"></i>';
        },
        goBack : function(){
            window.history.back(); //FIXME плохо работает в случаях когда назад уже некуда и может вернуть другой сайт
        },
        deleteDocument : function(){
            this.model.destroy().then(function(){
                App.router.navigate('/docs', {trigger : true});
            }.bind(this));
        },
        restoreDocument : function(){
            this.model.restore().then(function(){
                App.router.navigate('/trash', {trigger : true});
            });
        }
    });
});