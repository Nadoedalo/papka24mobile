/**
 * Created by dn27090siv on 05.12.2014.
 */
/*jslint nomen: true, regexp: true*/
define([
    'jquery',
    'underscore',
    'backbone',
    'js/app',
    'js/modules/EnterPassword/EnterPasswordView',
    'toastr'
], function ($, _, Backbone, App, EnterPasswordView, toastr) {
    "use strict";
    return Backbone.View.extend({
        tagName: 'div',
        className: 'DigitalSign',
        initialize: function () {
            App.userModel.on('change:userSettings', this.translateRenew, this); //required because router won't re-init digitalSign
            $.ajax({
                url : '/api/me/language'
            }).always(function(response){
                var matcher = {
                        UKR : 'uk-UK',
                        RUS : 'ru-RU'
                    },
                    language = matcher[response] || response;
                if(language !== 'uk-UK' || language !== 'ru-RU'){
                    language = App.userModel.get('userSettings').language;
                }
                this.getTranslates(language).done(function (json) {
                    this.glot.extend(json);
                    if ((App.userModel.get('userSettings').sign_type) === 'digital') {
                        this.getPluginInstance(true);
                    }
                }.bind(this));
            }.bind(this));
            return this;
        },
        events: {
            'click .getPath': 'getPath',
            'click .changePath': 'getPath'
        },
        translateRenew : function(model){
            if(model.attributes.userSettings.language !== model.previousAttributes().userSettings.language) {
                this.getTranslates().done(function (json) {
                    this.glot.extend(json);
                }.bind(this));
            }
        },
        getPluginInstance: function (toastrFlag, resetFlag) {
            var promise = new $.Deferred(),
                success = function (plugin) {
                    var cache = _.clone(this.model.get('functionCache'));
                    cache.getPluginInstance = true;
                    this.model.set('functionCache', cache);
                    if (this.pluginVersionCheck(toastrFlag)) {
                        promise.resolve(plugin);
                    } else {
                        promise.reject();
                    }
                }.bind(this),
                error = function (errorObj) {
                    _gaq.push(['_trackEvent', 'CryptoPlugin error', JSON.stringify(errorObj), '', 0]);
                    if(errorObj.code === -1){ // установка расширения для хрома
                        this.showInstallInfo().done(function(){
                            this.model.getPluginInstance()
                                .done(success)
                                .fail(error);
                        }.bind(this)).fail(function(){
                            promise.reject();
                        }.bind(this));
                    }else if (toastrFlag) {
                        this.showBlockedInfo();
                        promise.reject();
                    } else {
                        this.drawWarningPopup();
                        promise.reject();
                    }
                }.bind(this);
            if(this.model.get('functionCache').getPluginInstance && !this.pluginVersionCheck()){
                promise.reject();
                return promise;
            }
            if (!resetFlag && this.model.get('functionCache').getPluginInstance) {
                promise.resolve();
                return promise;
            }
            this.model.getPluginInstance()
                .done(success)
                .fail(error);
            return promise;
        },
        showLastDialog : function(key){
            var that = this,
                lastDialog = $('<p>', {
                    title: that.glot.t('ManualInstallTitle')
                }).html(that.glot.t(key));
            lastDialog.dialog({
                modal: true,
                width: '400px',
                buttons: [
                    {
                        text: that.glot.t('back'),
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ]
            });
        },
        showInstallInfo : function(){
            var that = this,
                promise = new $.Deferred(),
                dialog = $('<p>', {
                    title : this.glot.t('extensionRequiredTitle')
                }).html(this.glot.t('extensionRequired'));
            dialog.dialog({
                modal: true,
                width: '600px',
                close : function(){
                    promise.reject();
                    $(this).remove();
                },
                buttons: [
                    {
                        text : this.glot.t('back'),
                        click: function () {

                            $(this).dialog("close");
                        }
                    },
                    {
                        text: this.glot.t('installExtension'),
                        click: function () {
                            try {
                                if (window.navigator.userAgent.toLowerCase().indexOf('opr/') !== -1) { //Opera
                                    window.opr.addons.installExtension('aiikngbhbnkcahmaelhdfeaeenccfkej', function () { //плагин активирован
                                        promise.resolve();
                                    }, function () {
                                        that.showLastDialog('ManualInstallOpera');
                                        _gaq.push(['_trackEvent', 'Opera extension install error', JSON.stringify(arguments), ' ', 0]);
                                        promise.reject();
                                    });
                                } else {
                                    window.chrome.webstore.install('idfiabaafjemgcecklpgnebaebonghka', function () { //плагин активирован
                                        promise.resolve();
                                    }, function () {
                                        that.showLastDialog('ManualInstall');
                                        _gaq.push(['_trackEvent', 'Chrome extension install error', JSON.stringify(arguments), ' ', 0]);
                                        promise.reject();
                                    });
                                }
                            }catch(e){
                                var key = 'ManualInstall';
                                if (window.navigator.userAgent.toLowerCase().indexOf('opr/') !== -1) {
                                    key = 'ManualInstallOpera';
                                }
                                that.showLastDialog(key);
                                _gaq.push(['_trackEvent', 'Extension install error', JSON.stringify(e), '', 0]);
                                promise.reject();
                            }
                            $(this).dialog("close");
                        }
                    }
                ]
            });
            return promise;

        },
        pluginVersionCheck: function (noBlock) {
            var pluginVersion = +(''+this.model.get('pluginInstance').version).replace(/\./g, ''),
                currentVersion = +(''+this.model.get('current_version')).replace(/\./g, ''),
                minimumVersion = +(''+this.model.get('minimum_version')).replace(/\./g, ''),
                major = minimumVersion > pluginVersion,
                minor = currentVersion > pluginVersion,
                res = true;
            if (major && !noBlock) {
                window._gaq.push(['_trackEvent', 'Plugin Version Error', 'Blocked error', 'plugin : '+pluginVersion+' # minimum : '+minimumVersion+' # current : '+currentVersion, 0]);
                this.criticalOutdateAlert();
                res = false;
            } else if (minor || major) {
                window._gaq.push(['_trackEvent', 'Plugin Version Error', 'No Blocked error', 'plugin : '+pluginVersion+' # minimum : '+minimumVersion+' # current : '+currentVersion, 0]);
                this.outdateAlert();
            }
            return res;
        },
        drawWarningPopup: function () {
            var dialog = $('<p>', {
                title : this.glot.t('blockedTitle')
            }).html(this.glot.t('blocked'));
            dialog.dialog({
                modal: true,
                width: '600px',
                close : function(){
                    $(this).remove();
                },
                buttons: [
                    {
                        text: this.glot.t('close'),
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ]
            });
        },
        showBlockedInfo: function () {
            toastr.info(this.glot.t('blockedToastText', {
                linkPart: this.model.get('extension')[this.model.get('osType')]
            }));
        },
        openSession: function (resetFlag) {
            var promise = new $.Deferred(),
                success = function () {
                    var cache = _.clone(this.model.get('functionCache'));
                    cache.openSession = true;
                    this.model.set('functionCache', cache);
                    this.startUpdatingTokens();
                    promise.resolve.apply(this, arguments);
                }.bind(this),
                error = function () {
                    promise.reject.apply(this, arguments);
                }.bind(this);
            if (!resetFlag && this.model.get('functionCache').openSession) { //fixme seems like glitchy cache
                this.startUpdatingTokens();
                promise.resolve();
                return promise;
            }
            this.getPluginInstance()
                .done(function () {
                    this.model.openSession().done(success).fail(error);
                }.bind(this))
                .fail(error);
            return promise;
        },
        getPath: function (e) {
            if(e){
                e.preventDefault();
                e.stopPropagation();
            }
            var promise = new $.Deferred(),
                success = function () {
                    var path = _.clone(this.model.get('path'));
                    this.$el.find('.error').toggle(!path);
                    this.$el.find('.getPath').hide();
                    this.$el.find('#pathSelected').prop('checked', true).val(path);
                    this.$el.find('label[for=pathSelected]').attr('data-path', path).html(path.replace(/\//g, '\\').replace(/(^.+?\\).*(\\.*\\.*$)/, '$1...$2')).closest('div').show();
                    promise.resolve.apply(this, arguments);
                }.bind(this),
                error = function () {
                    promise.reject.apply(this, arguments);
                }.bind(this);
            this.model.getPath(this.glot.t('fileFormat'), '*.*', this.glot.t('SelectKeyStorageTitle'))
                .done(success)
                .fail(error);
            return promise;
        },
        getPassword: function (passType, selectedKey) {
            passType = passType || 'fillPassword';
            this.$el.remove();
            var wrongPassword = !!this.model.get('wrongPassword'),
                promise = new $.Deferred(),
                path = _.clone(this.model.get('path')).replace(/\//g, '\\').replace(/(^.+?\\).*(\\.*\\.*$)/, '$1...$2'),
                key = selectedKey || '',/*_.isEmpty(this.model.get('certSubject')) ? '' : this.model.get('certSubject'),*/
                temp = new EnterPasswordView({phrase: passType, wrongPassword: wrongPassword, key: key, path: path}),
                always = function () {
                    this.stopListening(temp);
                }.bind(this);
            promise.always(always);
            this.listenTo(temp, 'passwordEntered', promise.resolve);
            this.listenTo(temp, 'passwordReject', function(){
                promise.reject({
                    code : 'triggerBack',
                    type : 'getPassword'
                });
            });
            return promise;
        },
        activateFile: function (resetFlag) {
            this.model.set('busy', false);
            var promise = new $.Deferred(),
                passType,
                error = function () {
                    promise.reject.apply(this, arguments);
                }.bind(this),
                success = function () {
                    var cache = _.clone(this.model.get('functionCache'));
                    cache.activateFile = true;
                    this.model.set({
                        functionCache : cache,
                        busy : true
                    });
                    promise.resolve.apply(this, arguments);
                }.bind(this),
                passwordGot = function (password) {
                    this.model.activateFile(_.clone(this.model.get('path')), password, this.model.get('keyType')).done(success).fail(function (errorCodes) {
                        if(errorCodes.code === 4){
                            error.apply(this, arguments);
                            return false;
                        }
                        this.model.set('wrongPassword', true);
                        this.getPassword(passType)
                            .done(passwordGot)
                            .fail(error);
                    }.bind(this));
                }.bind(this);
            if (!resetFlag && this.model.get('functionCache').activateFile) {
                promise.resolve();
                return promise;
            }
            this.openSession().fail(error).done(function () {
                this.showActivateFile().fail(error).done(function (path, type) {
                    passType = type ? 'tokenPassword' : 'keyStoragePassword';
                    this.model.set('wrongPassword', false);
                    this.model.set({
                        'path': path,
                        'keyType': passType,
                        'busy' : true
                    });
                    this.getPassword(passType)
                        .done(passwordGot)
                        .fail(error);
                }.bind(this));
            }.bind(this));
            return promise;
        },
        showActivateFile: function () { //returns path on done
            var dialog,
                promise = new $.Deferred(),
                success = function () {
                    var selected = dialog.find('.selectKeyAlias:checked'),
                        path = selected.val(),
                        type = 'pathSelected' !== selected.attr('id');
                    if (!path) {
                        this.getPath().done(success).fail(function(){
                            promise.reject();
                        });
                    }else{
                        promise.resolve(path, type);
                    }
                }.bind(this);
            promise.always(function(){
                this.stopListening(this.model); //fixme can cause issues
            }.bind(this));
            dialog = $('<p>', {
                title: this.glot.t('chooseKey')
            }).html(this.getActivateFileHtml()).dialog({
                width: '600px',
                modal: true,
                close : function(){
                    promise.reject();
                    $(this).remove();
                },
                buttons: [
                    {
                        text: this.glot.t('back'),
                        click: function () {
                            promise.reject({
                                code : 'triggerBack',
                                type : 'activateFile'
                            });
                            $(this).remove();
                        }
                    },
                    {
                        text: this.glot.t('forward'),
                        click: success
                    }
                ]
            });
            this.$el = dialog;
            this.listenTo(this.model, 'change:LookingForTokens', function(model, value){
                this.$el.find('.looking_for_token').toggleClass('active', value).toggle(value);
            }.bind(this));
            this.listenTo(this.model, 'change:TokensHTML', function(model, value){
                this.$el.find('.tokens').html(value);
            }.bind(this));
            this.delegateEvents();
            return promise;
        },
        getActivateFileHtml: function () {
            var res = $('<div>'),
                tempImg = $('<div>', {class: 'looking_for_token firstDisplayNone'}),
                path = this.model.get('keyType') === 'tokenPassword' ? _.clone(this.model.get('savedPath')) : _.clone(this.model.get('path'));
            tempImg.append(
                this.glot.t('searchForTokens'),
                $('<img>', {
                    src: '/{{%version%}}/design/img/loop.png',
                    class: 'animated'
                })
            );
            res.append(
                $('<div>', {
                    class:'tokens paddingLeft3em'
                }).append(this.model.get('TokensHTML')),
                $('<div>', {
                    class : 'paddingLeft3em'
                }).append(
                    $('<a>', {
                        class: 'chooseStorageButton getPath',
                        text: this.glot.t('chooseStorage')
                    }),
                    $('<div>', {class: 'keyPath firstDisplayNone'}).append(
                        $('<input>', {
                            type: 'radio',
                            name: 'selectDeviceAlias',
                            class: 'selectKeyAlias',
                            id: 'pathSelected',
                            value: ''
                        }),
                        $('<label>', {
                            class: 'userKeySelect',
                            for: 'pathSelected'
                        }),
                        $('<a>', {
                            href: '#',
                            class: 'changePath marginLeft05em',
                            text: this.glot.t('changePath')
                        })
                    ),
                    tempImg,
                    $('<p>', {class: 'error firstDisplayNone'}).html(this.glot.t('chooseKeyStorage'))
                )
            );
            if(path){
                res.find('.error').toggle(!path);
                res.find('.getPath').hide();
                res.find('#pathSelected').prop('checked', true).val(path);
                res.find('label[for=pathSelected]').attr('data-path', path).html(path.replace(/\//g, '\\').replace(/(^.+?\\).*(\\.*\\.*$)/, '$1...$2')).closest('div').show();
            }
            res.find('input').first().attr({checked: 'checked', autofocus: 'autofocus'});
            return res;
        },
        selectKey: function (resetFlag) {
            var promise = new $.Deferred(),
                alias = '',
                passType = 'keyPassword',
                selectedKey,
                success = function () {
                    var cache = _.clone(this.model.get('functionCache'));
                    cache.selectKey = true;
                    this.model.set('functionCache', cache);
                    promise.resolve.apply(this, arguments);
                }.bind(this),
                error = function () {
                    promise.reject.apply(this, arguments);
                }.bind(this),
                checkPassword,
                wrongPassword;
            checkPassword = function (password) {
                this.model.get('pluginInstance').selectKey(alias, password, success, wrongPassword);
            }.bind(this);
            wrongPassword = function (errorCodes) {
                if(errorCodes.level === 0 && errorCodes.code === 4){
                    error.apply(this, arguments);
                    return false;
                }
                this.model.set('wrongPassword', true);
                this.getPassword(passType, selectedKey).done(checkPassword).fail(error);
            }.bind(this);
            if (!resetFlag && this.model.get('functionCache').selectKey) {
                promise.resolve();
                return promise;
            }
            this.activateFile().done(function () {
                this.showSelectKey().done(function (keyAlias) {
                    selectedKey = _.find(this.model.get('keys'), function (key) {
                        return key.alias === keyAlias;
                    });
                    alias = selectedKey.alias;
                    if (!selectedKey.needPassword) {
                        this.model.get('pluginInstance').selectKey(alias, '', success, error);
                    } else {
                        this.model.set('wrongPassword', false);
                        this.getPassword(passType, selectedKey).done(checkPassword).fail(error);
                    }
                }.bind(this)).fail(error);
            }.bind(this)).fail(error);
            return promise;
        },
        showSelectKey: function () {
            var promise = new $.Deferred(),
                temp = $('<p>', {title: this.glot.t('chooseKey'), class: "showSelectKeyPadding"}),
                promiseArr = [],
                cache = _.clone(this.model.get('functionCache')),
                success = function () {
                    temp = this.$el.find('.selectKeyAlias:checked');
                    cache.getKeyAlias = true;
                    this.model.set({
                        'functionCache': cache,
                        'keyAlias': temp.val(),
                        'keyText': temp.closest('div').find('label').text(),
                        'certSubject': _.extend(JSON.parse(temp.closest('div').find('label').attr('data-cert')), {alias: temp.val()})
                    });
                    promise.resolve(temp.val());
                }.bind(this),
                error = function () {
                    promise.reject.apply(this, arguments);
                }.bind(this);
            promise.always(function () {
                this.$el.remove();
            }.bind(this));
            this.model.getKeys().done(function (keys) {
                _.each(keys, function (val) {
                    promiseArr.push(val.alias);
                }.bind(this));
                this.getSubjectsByRecursion(promiseArr).done(function (arr) {
                    var countOfKeys = (arr.length || 0);
                    _.each(arr, function (obj, i) {
                        var divSelectOneKey = $('<div>').append($('<input>', {
                                type: 'radio',
                                name: 'selectKeyAlias',
                                class: 'selectKeyAlias',
                                value: keys[i].alias,
                                id: keys[i].alias
                            })),
                            label4OneKey = $('<label>', {class: 'userKeySelect showBlockEl', 'for': keys[i].alias, 'data-cert': JSON.stringify(obj)});
                        label4OneKey.append(
                            $('<div>', {class: 'keyInfoLeftPart', title: obj.T}).text(obj.T),
                            $('<div>').append(
                                $('<div>', {class: "keyInfoFio", title: obj.CN}).text(obj.CN),
                                $('<div>').text(obj.O)
                            )
                        );
                        divSelectOneKey.append(label4OneKey);
                        if (i < countOfKeys - 1) {
                            divSelectOneKey.append($('<div>', {class: 'hrSeparator'}));
                        }
                        temp.append(divSelectOneKey);
                    });
                    temp.find('input').first().attr({checked: 'checked'});
                    this.$el.html(temp).dialog({
                        modal: true,
                        width: '600px',
                        close : function(){
                            promise.reject();
                            $(this).remove();
                        },
                        buttons: [
                            {
                                text: this.glot.t('back'),
                                click: function(){
                                    promise.reject({
                                        code : 'triggerBack',
                                        type : 'showSelectKey'
                                    });
                                    $(this).remove();
                                }
                            },
                            {
                                text: this.glot.t('forward'),
                                click: success,
                                id: 'otp_prove_ok'
                            }
                        ]
                    });
                }.bind(this));
            }.bind(this)).fail(error);
            return promise;
        },
        getSubjectsByRecursion : function(arr){
            var promise = new $.Deferred(),
                resultArr = [],
                recursion = function(){
                    var firstItem = arr.shift();
                    if(firstItem){
                        this.model.getCertificateSubject(firstItem).done(function(item){
                            resultArr.push(item);
                            recursion();
                        }).fail(promise.reject);
                    }else{
                        promise.resolve(resultArr);
                    }

                }.bind(this);
            recursion();
            return promise;
        },
        showError : function(error){
            var text = '',
                temp;
            if(error){
                if(error.code === 4){
                    text = this.model.get('keyType') ? 'repeatToken' : 'repeatKey';
                }
                this.model.resetToDefaults();
                temp = $('<p>', {
                    title : this.glot.t('errorTitle')
                }).html(this.glot.t('defaultError', {
                        message : error.message +'. ',
                        code : error.code
                    }) + ' '+text);
                temp.dialog({
                    modal: true,
                    width: '600px',
                    close : function(){
                        $(this).dialog("destroy").remove();
                    },
                    buttons: [
                        {
                            text: this.glot.t('close'),
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ]
                });
            }
        },
        cmsSign : function(data, hashParams, certificate, timeStamp, includeCertificate, includeData){ //method with JSON
            data = JSON.stringify(data);
            return this._cmsSign(data, hashParams, certificate, timeStamp, includeCertificate, includeData);
        },
        cmsSignFile : function(){
            return this._cmsSign.apply(this, arguments);
        },
        _cmsSign: function (data, hashParams, certificate, timeStamp, includeCertificate, includeData) {
            var promise = new $.Deferred(),
                dataArr = [data, hashParams, certificate, timeStamp, includeCertificate, includeData],
                error,
                success;
            error = function (obj) {
                if(obj && obj.code === 'triggerBack'){
                    this.handleBackButton.apply(this, arguments).done(function(){
                        this._cmsSign.apply(this, dataArr).done(success).fail(error);
                    }.bind(this)).fail(error);
                } else {
                    this.showError.apply(this, arguments);
                    this.trigger('errorDigitalSign');
                    promise.reject();
                }
            }.bind(this);
            success = function () {
                data = this.model.btoa(data);
                hashParams = hashParams || '';//fill it, must be the same as in getTimeStampReq
                certificate = certificate || this.model.get('certificate');
                includeCertificate = /*!!includeCertificate*/ true;
                includeData = /*includeData || */true;
                var allSuccess = function (sign) {
                    promise.resolve(sign);
                    this.trigger('successDigitalSign', sign);
                }.bind(this);
                this.model.unset('getTimeStampReq', {silent: true});
                this.model.unset('TimeStampReq', {silent: true});
                this.model.getTimeStampReq(data)
                    .done(function (superTimeStamp) {
                        this.model.get('pluginInstance').CMSSign(data, hashParams, certificate, timeStamp || superTimeStamp, includeCertificate, includeData, allSuccess, error);
                    }.bind(this))
                    .fail(error);
            }.bind(this);
            promise.always(function () {
                this.$el.remove();
            }.bind(this));
            this.checkSession().done(function(){
                this.selectKey().done(success).fail(error);
            }.bind(this));
            return promise;
        },
        outdateAlert: function () {
            toastr.warning(this.glot.t('outdatedPlugin', {
                linkPart: this.model.get('extension')[this.model.get('osType')]
            }));
        },
        criticalOutdateAlert: function () {
            var temp = $('<p>', {
                title : this.glot.t('outdatedPluginTitle')
            }).html(this.glot.t('criticallyOutdated', {
                version: this.model.get('pluginInstance').version,
                linkPart: this.model.get('extension')[this.model.get('osType')],
                current_version: this.model.get('current_version')
            }));
            temp.dialog({
                modal: true,
                width: '600px',
                close : function(){
                    $(this).dialog("destroy").remove();
                },
                buttons: [
                    {
                        text: this.glot.t('close'),
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ]
            });
        },
        checkSession : function(){
            var promise = new $.Deferred();
            if(this.model.get('functionCache').getPluginInstance && !this.model.get('pluginInstance').getTimeStampReq){
                this.model.set('functionsCache', {});
            }
            return promise.resolve();
        },
        startUpdatingTokens : function () {
            var that = this.model,
                thatView = this,
                success,
                error,
                iterator = function () {
                    if(!that.get('busy') && that.get('functionCache').openSession && that.get('pluginInstance')){
                        that.set('LookingForTokens', true);
                        that.get('pluginInstance').getDeviceList(success, error);
                    }else{
                        thatView.setTimeoutsOnce(iterator, 3000);
                    }
                };
            success = function (deviceArr) {
                var res = $('<div>');
                _.each(deviceArr, function (val) {
                    res.append(
                        $('<div>').append(
                            $('<input>', {
                                type: 'radio',
                                name: 'selectDeviceAlias',
                                class: 'selectKeyAlias',
                                value: val.deviceName || val.deviceId,
                                id: val.deviceId
                            }),
                            $('<label>', {class: 'userKeySelect', 'for': val.deviceId, 'data-key': JSON.stringify(val)}).html(val.deviceName || val.deviceId)
                        )
                    );

                });
                that.set({
                    LookingForTokens: false,
                    TokensHTML: res.wrap('<div>').html()
                });
                thatView.setTimeoutsOnce(iterator, 3000);
            };
            error = function () {
                this.setTimeoutsOnce(this.startUpdatingTokens.bind(this), 5000);
            }.bind(this);
            iterator();
            return this;
        },
        setTimeoutsOnce : function(func, delay){
            var temp = this.model.get('timeoutTimer');
            _.each(temp, function(val){
                window.clearTimeout(val);
            });
            temp = window.setTimeout(func, delay);
            this.model.set('timeoutTimer', [temp]);
        },
        handleBackButton : function(obj){
            var promise = new $.Deferred(),
                steps = ["activateFile", "showSelectKey"], //текущие функции в порядке возможности кнопки назад по порядку fixme должно быть в аттрибутах
                functionCache,
                i;
            if(obj.type === 'getPassword'){
                promise.resolve();
            }else if((i = steps.indexOf(obj.type)) > 0){
                this.forgetStorage().done(function(){ //fixme только showSelectKey может быть больше 0 потому эта логика - для него
                    functionCache = this.model.get('functionCache');
                    delete functionCache[steps[i]];
                    delete functionCache[steps[i - 1]];
                    this.model.set('functionCache', functionCache);
                    promise.resolve();
                }.bind(this));

            }else{
                promise.reject();
            }
            return promise;
        },
        forgetEverything : function(){
            return this.model.forgetEverything();
        },
        forgetStorage : function(){ //fixme mode to model
            return this.forgetEverything();
        },
        forgetKey : function(){ //fixme mode to model
            //TODO fixme при переключении ключа забывать предидущий ключ
        }
    });
});