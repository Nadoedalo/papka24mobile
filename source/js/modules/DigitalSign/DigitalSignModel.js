/*
* Бизнес логика :
* getPluginInstance -> openSession -> getPath(опционально, если есть base64 то пропустить)
* -> activateFile -> getKeys -> getCertificateSubject -> selectKey(какого-то хрена во вью)
* -> getTimeStampReq -> получение метки времени от запроса -> CMSSign
* */
define([
    'underscore',
    'backbone',
    'CryptoPlugin',
    'js/app'
], function (_, Backbone, CryptoPlugin, App) {
    'use strict';
    return Backbone.Model.extend({
        url : '',
        defaults : {
            functionCache : {},
            referenceId : 0, //TODO изменить название на корректное
            path : '',
            steps : []
        },
        initialize : function(){
            this.set({
                'path' : this.getPathFromLocalStorage()
            });
            this.on('change:path', this.savePathToLocalStorage, this);
            return this;
        },
        getPathFromLocalStorage : function(){
            return localStorage.getItem('DigitalSignStoragePath');
        },
        savePathToLocalStorage : function(){
            localStorage.setItem('DigitalSignStoragePath', JSON.stringify(this.get('path')));
            return true;
        },
        resetToDefaults : function(){
            this.clear({silent : true});
            this.set(this.defaults, {silent : true});
            this.off();
            this.stopListening();
            this.initialize();
            return this;
        },
        getPluginInstance : function(){
            return new Promise(function(resolve, reject){
                var success = function(obj){
                        this.set({
                            'pliginInstance' : obj
                        });
                        resolve(obj);
                    }.bind(this),
                    error = function(errorText){
                        reject(errorText);
                    };
                CryptoPlugin.config.debugFunction = this.debugPlugin;
                CryptoPlugin.connect(success, error);
            }.bind(this));
        },
        debugPlugin : function (text, type) {
            if (type === "info") {
                console.log(text);
            } else {
                console.warn(text);
            }
        },
        openSession : function(){
            return new Promise(function(resolve, reject){
                var success = function(session_id){
                        this.set({
                            session_id : session_id
                        });
                        resolve(session_id)
                    }.bind(this),
                    error = function(){reject.apply(this, arguments);},
                    plugin = this.get('pluginInstance');
                if(plugin){
                    plugin.openSession(900, success, error);
                }else{
                    error('No pluginInstance');
                }
            }.bind(this));
        },
        getPath : function(fileTypeText, wildcard, title){
            /*
            * TODO
            * Необходим выбор источника(из файла, из облака и т.п)
            * т.к дибильные маководы не умеют в выбор файла
            * И вообще этот шаг должен разруливаться на вьюхе
            * */
            return new Promise(function(resolve, reject){
                var success = function(path){
                        this.set({
                            'path' : path.filePath
                        });
                        resolve(path);
                    },
                    error = function(){reject.apply(this, arguments);},
                    pattern = JSON.stringify([{"text":fileTypeText,"pattern":wildcard}]);
                this.get('pluginInstance').selectFile('', pattern, title, success, error);
            }.bind(this));
        },
        activateFile : function(path, password, type){
            return new Promise(function(resolve, reject){
                var success = function(){resolve.apply(this, arguments);},
                    error = function(){reject.apply(this, arguments);},
                    plugin = this.get('pluginInstance');
                if(plugin){
                    this.get('pluginInstance').selectFileStorage(path, password, success, error);
                }else{
                    error('No pluginInstance');
                }
            }.bind(this));
        },
        getKeys : function(){
            return new Promise(function(resolve, reject){
                var success = function(keys){
                        this.set('keys', keys);
                        resolve.apply(this, arguments);
                    }.bind(this),
                    error = function(){reject.apply(this, arguments);};
                this.get('pluginInstance').getKeysList(success, error);
            }.bind(this));
        },
        getCertificateSubject : function(alias){
            return new Promise(function(resolve){
                var plugin = this.get('pluginInstance');
                plugin.getCertificateChain(alias, function(obj){
                    plugin.getCertificateInfo(obj[0], function(obj){
                        var extendKey = _.find(this.get('keys'), function (key) {
                            return key.alias === alias;
                        });
                        _.extend(extendKey, obj.subject);
                        resolve(obj.subject);
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        },
        getTimeStampReq : function(data){
            return new Promise(function(resolve, reject){
                var error = function(){
                        reject({
                            message : 'Ошибка получения метки времени',
                            code : '0'
                        });
                    },
                    success = function(time){
                        this.save({}, {
                            parse : false,
                            url : '/api/acsk/tsp',
                            data : time.timeStampReq,
                            dataType : 'text',
                            contentType : 'text/plain',
                            error : error
                        }).then(function(correctTimeStamp){
                            this.set('TimeStampReq', correctTimeStamp);
                            resolve(correctTimeStamp);
                        }.bind(this), error);
                    }.bind(this);
                this.get('pluginInstance').getTimeStampReq(data, '', true, '1.2.804.2.1.1.1.2.3.1', success, error); // data(same as in cmsSign), hashParam(same as in cmsSign), certReq, reqPolicym nonce*(may be optional)
            }.bind(this));
        },
        btoa : function(str){
            var Base64 = {
                _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
                encode: function (e) {
                    var t = "", n, r, i, s, o, u, a, f = 0;
                    e = Base64._utf8_encode(e);
                    while (f < e.length) {
                        n = e.charCodeAt(f++);
                        r = e.charCodeAt(f++);
                        i = e.charCodeAt(f++);
                        s = n >> 2;
                        o = (n & 3) << 4 | r >> 4;
                        u = (r & 15) << 2 | i >> 6;
                        a = i & 63;
                        if (isNaN(r)) {
                            u = a = 64
                        } else if (isNaN(i)) {
                            a = 64
                        }
                        t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
                    }
                    return t;
                },
                _utf8_encode: function (e) {
                    e = e.replace(/\r\n/g, "\n");
                    var t = "";
                    for (var n = 0; n < e.length; n++) {
                        var r = e.charCodeAt(n);
                        if (r < 128) {
                            t += String.fromCharCode(r)
                        } else if (r > 127 && r < 2048) {
                            t += String.fromCharCode(r >> 6 | 192);
                            t += String.fromCharCode(r & 63 | 128)
                        } else {
                            t += String.fromCharCode(r >> 12 | 224);
                            t += String.fromCharCode(r >> 6 & 63 | 128);
                            t += String.fromCharCode(r & 63 | 128)
                        }
                    }
                    return t
                }
            };
            return Base64.encode(str);
        },
        clearCache : function(){ // unnecessary when you have reset to defaults
            this.set('functionCache', {});
            return this;
        },
        forgetEverything : function(){
            var always = function(){
                this.clearCache();
                this.set({
                    'path' : ''
                });
            }.bind(this);
            return this.forgetStorage().then(always, always);
        },
        forgetStorage : function(){
            return new Promise(function(resolve, reject){
                this.get('pluginInstance').closeSession(resolve, reject);
            });
        }
    });
});