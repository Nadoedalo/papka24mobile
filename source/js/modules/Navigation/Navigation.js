define([
    'underscore',
    'backbone',
    'js/app',
    'js/modules/TagNavList/TagNavListView'
], function (_, Backbone, App, TagNavListView) {
    "use strict";
    return Backbone.NativeView.extend({
        tagName: 'div',
        className: 'navigation',
        events: {
            "click a" : 'handleHrefClick'
        },
        initialize: function () {
            this.tagNavListView = new TagNavListView({model : App.userModel});
            this.listenTo(App.router, 'route', this.handleRoute);
            this.listenTo(this.tagNavListView, 'handleActive', this.handleRoute);
            this.render();
            return this;
        },
        render: function () {
            this.template().then(function (html) {
                this.el.innerHTML = html;
                var node = this.el.querySelector('.tagsNavList');
                node.parentNode.replaceChild(this.tagNavListView.el, node);
                this.handleRoute();
            }.bind(this));
            return this;
        },
        handleHrefClick : function(e){
            e.stopPropagation();
            e.preventDefault();
            var target = e.target,
                href = target.getAttribute('href');
            App.router.navigate(href, {trigger : true});
            document.querySelector('.menu.menu--visible').classList.remove('menu--visible');
        },
        handleActive : function(target){
            _.each(this.el.querySelectorAll('.active'), function(val){
                val.classList.remove('active');
            });
            var primary = this.el.querySelector('.primary');
            if(!target){
                if(primary){
                    primary.classList.add('active');
                }
                return;
            }
            if(/\/tag\/\d+$/.test(target.getAttribute('href'))){
                _.each(this.el.querySelectorAll('.active'), function(val){
                    val.classList.remove('active');
                });
                primary.classList.add('active');
            }
            target.classList.add('active');
        },
        handleRoute : function(){
            var current = App.router.current(),
                target = this.el.querySelector('a[href="/'+current.fragment+'"]');
            this.handleActive(target);
        }
    });
});