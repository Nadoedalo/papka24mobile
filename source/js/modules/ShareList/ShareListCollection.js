define([
    'underscore',
    'backbone',
    'js/app'
], function (_, Backbone, App) {
    "use strict";
    return Backbone.Collection.extend({
        url: '',
        initialize: function (models, options) {
            this.referenceModel = options.referenceModel;
            this.listenTo(this.referenceModel, 'change:shares', this.updateShares, this);
            this.listenTo(App.router, 'route', function(){this.stopListening();});
            return this;
        },
        addShares : function(emailsArr, comment, mode){
            if(!emailsArr || !_.isArray(emailsArr) || emailsArr.length < 1){return false;}
            return this.referenceModel.addShares(emailsArr, comment, mode);
        },
        getShares : function(){
            return _.keys(this.referenceModel.get('shares'));
        },
        updateShares : function(model, shares){
            var tempShares = _.clone(shares);
            if(this.referenceModel.get('author') !== App.userModel.get('login')){
                delete tempShares[App.userModel.get('login')];
                tempShares[this.referenceModel.get('author')] = this.referenceModel.get('deleteByOwner') ? '999999998' : '999999999';
            }
            tempShares = _.reduce(tempShares, function (memo, val, key) {
                memo.push({
                    email: key,
                    status: val
                });
                return memo;
            }, []);
            this.reset(tempShares);
        }
    });
});