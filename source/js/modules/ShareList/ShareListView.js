define([
    'underscore',
    'backbone',
    'js/app',
    'md5',
    'sha256'
], function (_, Backbone, App) {
    "use strict";
    return Backbone.NativeView.extend({
        tagName: 'div',
        className: 'shareListView',
        events: {
            'click .delete' : 'removeShare'
        },
        initialize: function () {
            this.render();
            this.model.on('change', this.render, this);
            this.model.on('remove destroy', this.remove, this);
            return this;
        },
        render: function () {
            var email = this.model.get('email'),
                status = this.model.get('status'),
                data = {
                    email : email,
                    img : '/cdn/avatars/'+Sha256.hash(email)+'.png',
                    failImg : 'https://secure.gravatar.com/avatar/'+ MD5(email) + '?d=mm',
                    deleteFlag : status % 10 <= 1,
                    statusTextCode : this.model.get('status'),
                    name : App.userModel.get('friends')[email] || email
                };
            this.template(data, '', false).then(function (html) {
                this.el.innerHTML = html;
            }.bind(this));
            return this;
        },
        removeShare : function(){
            return this.model.destroy();
        }
    });
});