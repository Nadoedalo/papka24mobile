define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    "use strict";
    return Backbone.Model.extend({
        url: '',
        defaults : {
            
        },
        initialize: function () {
            return this;
        }
    });
});