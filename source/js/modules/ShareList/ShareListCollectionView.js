define([
    'underscore',
    'backbone',
    'js/modules/ShareList/ShareListView',
    'js/modules/AddShare/AddShareCollection',
    'js/modules/AddShare/AddShareModel',
    'js/modules/AddShare/AddShareCollectionView'
], function (_, Backbone, ShareListView, AddShareCollection, AddShareModel, AddShareCollectionView) {
    "use strict";
    return Backbone.NativeView.extend({
        tagName: 'div',
        className: 'shareListCollectionView',
        events: {
            'click .shareListTitle' : 'toggleContent',
            'click .addShare' : 'showAddShare',
            'click .undoAddShare' : 'undoAddShare',
            'click .sendShare' : 'sendShare'
        },
        initialize: function () {
            this.collection.on('change update reset', this.render, this);
            this.collection.on('add', this.addOne, this);
            this.addShareCollection = new AddShareCollection([], {model : AddShareModel});
            this.addShareCollectionView = {};
            this.render();
            return this;
        },
        render: function () {
            var data = {
                length : this.collection.length
            };
            this.template(data, '', false).then(function (html) {
                this.el.innerHTML = html;
                _.each(this.collection.models, this.addOne, this);
                this.addShareCollectionView = new AddShareCollectionView({collection : this.addShareCollection, referenceModel : this.collection.referenceModel});
                var node = this.el.querySelector('.addShares');
                node.parentNode.replaceChild(this.addShareCollectionView.el, node);
            }.bind(this));
            return this;
        },
        addOne: function (model) {
            var view = new ShareListView({
                model: model
            });
            this.el.querySelector('.shareListContent').appendChild(view.el);
            return this;
        },
        toggleContent : function(){
            this.el.querySelector('.shareListContent').classList.toggle('hidden');
        },
        showAddShare : function(){
            _.each(this.el.querySelectorAll('.shareListContent, .shareListContentControls'), function(val){
                val.classList.add('hidden');
            });
            this.el.querySelector('.addSharesControls').classList.remove('hidden');
            this.addShareCollectionView.show();
        },
        undoAddShare : function(){
            _.each(this.el.querySelectorAll('.shareListContent, .shareListContentControls'), function(val){
                val.classList.remove('hidden');
            });
            this.el.querySelector('.addSharesControls').classList.add('hidden');
            this.addShareCollectionView.hide();
            /*TODO remove all shares found?*/
        },
        sendShare : function(){
            var sharesArr = this.addShareCollection.models.map(function(val){return val.attributes.email;}),
                comment = '',
                mode = 0;
            this.collection.addShares(sharesArr, comment, mode);
        }
    });
});