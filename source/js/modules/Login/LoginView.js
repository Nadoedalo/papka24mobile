define([
    'underscore',
    'backbone',
    'js/app'
], function (_, Backbone, App) {
    "use strict";
    return Backbone.NativeView.extend({
        tagName: 'div',
        className: 'login',
        events: {
            'submit' : 'auth'
        },
        initialize: function () {
            this.render();
            return this;
        },
        render: function () {
            this.template().then(function (html) {
                this.el.innerHTML = html;
            }.bind(this));
            return this;
        },
        auth : function(e){
            e.preventDefault();
            var obj = {
                login : this.el.querySelector('input[type=text]').value,
                password : this.el.querySelector('input[type=password]').value
            };
            this.model.auth(obj)
                .then(this.successLogin.bind(this), this.failLogin.bind(this));
        },
        successLogin : function(){
            App.router.navigate('/docs', {trigger : true});
        },
        failLogin : function(){
            //TODO error handling!
            console.error('error occured while trying to login', arguments);
            return false;
        }
    });
});