define([
    'underscore',
    'backbone',
    'md5',
    'sha256'
], function (_, Backbone, MD5, Sha256) {
    "use strict";
    return Backbone.NativeView.extend({
        tagName: 'div',
        className: 'userProfile',
        events: {
            'click .settings' : 'settings',
            'click .logout' : 'logout'
        },
        initialize: function () {
            this.model.on('change', this.render, this);
            this.render();
            return this;
        },
        render: function () {
            var data = _.clone(this.model.attributes);
            data.name = data.fullName || data.login;
            data.failImg = 'https://secure.gravatar.com/avatar/'+ MD5(data.login) + '?d=mm';
            data.img = '/cdn/avatars/'+Sha256.hash(data.login)+'.png';
            this.template(data, '', false).then(function (html) {
                this.el.innerHTML = html;
            }.bind(this), this._promiseErrorCatcher.bind(this));
            return this;
        },
        settings : function(){
            console.log('not implemented yet'); //TODO
        },
        logout : function(){
            this.model.logout().then(function(){
                localStorage.clear();
                window.location.reload();
            });
        }
    });
});