define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    "use strict";
    return Backbone.Model.extend({
        url: '',
        initialize: function () {
            return this;
        }
    });
});