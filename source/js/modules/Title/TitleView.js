define([
    'underscore',
    'backbone',
    'js/app'
], function (_, Backbone, app) {
    "use strict";
    return Backbone.NativeView.extend({
        tagName : 'div',
        className : 'titleView',
        initialize : function(){
            this.getTranslates('', 'TitleView').then(function(json){
                this.glot.extend(json);
                this.listenTo(app.router, 'route', this.render);
                this.render();
            }.bind(this));
            return this;
        },
        render : function(){
            var obj = app.router.current(),
                phraseCode = obj.route,
                partText = obj.params[0] || '',
                finalPhrase;
            if(!phraseCode && !this.glot.phrases[phraseCode]){
                phraseCode = 'default';
            }
            /*if(partText.length > 8){
                partText = partText.replace(/^(.{0,8}).*!/g, '$1*');
            }*/
            finalPhrase = this.glot.t(phraseCode, {ref : partText});
            document.querySelector('title').innerHTML = 'Папка24 '+finalPhrase; //fixme?
            this.el.innerHTML = finalPhrase;
            return this;
        }
    });
});