define([
    'underscore',
    'backbone',
    'fancyAutocomplete',
    'js/app',
    'js/modules/AddShare/AddShareView'
], function (_, Backbone, FancyAutocomplete, App, View) {
    "use strict";
    return Backbone.NativeView.extend({
        tagName: 'div',
        className: 'addShares hidden',
        events: {
            'click .addShareOption' : 'addShareOptionBut'
        },
        initialize: function (options) {
            this.referenceModel = options.referenceModel;
            this.alreadyShared = this.getSharesFromRef() || [];
            this.collection.on('add', this.addOne, this);
            this.collection.on('remove destroy', this.updateSelectedValues, this);
            this.render();
            return this;
        },
        render: function () {
            this.template('', 'addShares', true).then(function (html) {
                this.el.innerHTML = html;
                _.each(this.collection.models, this.addOne, this);
                this.initAutocomplete();
                this.updateAlreadyShared();
                this.listenTo(this.referenceModel, 'change:shares', this.updateAlreadyShared);
                this.listenTo(App.router, 'route', function(){this.stopListening();});
            }.bind(this));
            return this;
        },
        updateAlreadyShared : function(){
            this.alreadyShared = this.getSharesFromRef();
            this.updateSelectedValues();
        },
        getSharesFromRef : function(){
            var shares = _.clone(this.referenceModel.get('shares'));
            shares[App.userModel.get('login')] = 999999999;
            shares[this.referenceModel.get('author')] = 999999999;
            return _.keys(shares);
        },
        addOne: function (model) {
            var view = new View({
                    model: model
                }),
                node = this.el.querySelector('.shareOptions');
            node.insertBefore(view.el, node.childNodes[0]);
            return this;
        },
        show : function(){
            this.el.classList.remove('hidden');
        },
        hide : function(){
            this.el.classList.add('hidden');
        },
        addShareOption : function(emailsArr){
            if(!emailsArr || !_.isArray(emailsArr)){return;}
            emailsArr = emailsArr.map(function(val){return {email : val};});
            this.collection.add(emailsArr, {validate : true});
            this.el.querySelector('.shareInput').value = '';
            this.updateSelectedValues();
        },
        updateSelectedValues : function(){
            var arr = this.collection.models.map(function(val){return val.attributes.email;}); //FIXME вот жешь я мудак нет что бы по человечески сделать
            arr = _.union(arr, this.alreadyShared);
            this.customComplete.selectedValues = arr;
        },
        initAutocomplete : function(){
            var customComplete,
                mainArr = this.collection.buildAutocompleteList(),
                keys = {
                    valueKeys : ['email'],
                    renderKeys : ['email', 'name', 'company']
                };
            customComplete = new FancyAutocomplete(mainArr, this.el.querySelector('.shareInput'), keys, true);
            customComplete.callbackSearch = this.addShareOption.bind(this);
            customComplete.updateTargetValue = function(){};
            customComplete.parentTarget = function(){
                return this.target.parentElement.parentElement;
            };
            customComplete.addWraps = function(){
                this.autocompleteWrap = this.parentTarget().querySelector('.autoCompleteWrap');
                this.addClass(this.target, this.className);
                if(!this.autocompleteWrap){
                    this.autocompleteWrap = document.createElement('div');
                    this.addClass(this.autocompleteWrap, 'autoCompleteWrap');
                    this.parentTarget().insertBefore(this.autocompleteWrap, this.target.parentElement.nextSibling);
                }else{
                    this.autocompleteWrap.innerHTML = '';
                }
            };
            customComplete.setSelectedValues = function(){
                var arr = this.target.value.split(',').map(function(item){
                        return item.replace(/^["\s]+|["\s]+$/g, '');
                    }),
                    length = arr.length,
                    i,
                    res = [];
                for(i = 0; i < length; ++i){ //_.compact
                    if(arr[i]){
                        res.push(arr[i]);
                    }
                }
                return res.length ? res : '';
            };
            var that = this;
            customComplete.addJSactive = function(target){
                if(!target){
                    that.el.querySelector('.shareInputWrapper').classList.toggle('js-active', true);
                }
                this.addClass(target || this.target, 'js-active');
            };
            customComplete.rmJSactive = function(target){
                if(!target){
                    that.el.querySelector('.shareInputWrapper').classList.toggle('js-active', false);
                }
                this.rmClass(target || this.target, 'js-active');
            };
            this.customComplete = customComplete;
            this.customComplete.initialize();
            window.customComplete = customComplete;
        },
        addShareOptionBut : function(){
            var arr = this.el.querySelector('.shareInput').value.split(',').map(function(val){
                    val = val.replace(/^["\s]+|["\s]+$/g, '');
                    if(this.alreadyShared.indexOf(val) != -1){
                        return;
                    }
                    return val;
                }.bind(this));
            this.addShareOption(
                _.compact(arr)
            );
        }
    });
});