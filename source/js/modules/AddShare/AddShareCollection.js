define([
    'underscore',
    'backbone',
    'js/app'
], function (_, Backbone, App) {
    "use strict";
    return Backbone.Collection.extend({
        url: '',
        initialize: function () {
            this.buildAutocompleteList();
            return this;
        },
        buildAutocompleteList : function(){
            var res = [],
                collectionRes = {},
                friends = App.userModel.get('friends'),
                company = '';
            _.each(friends, function(val, key){
                company = App.userModel.getFriendsCompanyByEmail(key) || '';
                res.push({
                    email : key,
                    name : val,
                    company : company
                });
                collectionRes[key] = {name : val, company : company};
            });
            this.friendsHashTable = collectionRes;
            return res;
        },
        getAttributesByEmail : function(email){
            return this.friendsHashTable[email];
        }
    });
});