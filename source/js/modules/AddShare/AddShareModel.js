define([
    'underscore',
    'backbone',
    'sha256',
    'md5'
], function (_, Backbone, Sha256, MD5) {
    "use strict";
    return Backbone.Model.extend({
        idAttribute : 'email',
        defaults : {
            email : null,
            name : '',
            company : '',
            img : '',
            failImg : ''
        },
        validate : function(attr){
            var emailRegexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(!emailRegexp.test(''+attr.email)){
                return {
                    code : 1,
                    text : 'invalid email'
                };
            }
        },
        initialize: function () {
            var email = this.get('email'),
                obj = this.collection.getAttributesByEmail(email);
            obj.img = '/cdn/avatars/'+Sha256.hash(email)+'.png';
            obj.failImg = 'https://secure.gravatar.com/avatar/'+ MD5(email) + '?d=mm';
            this.set(
                obj
            );
            return this;
        }
    });
});