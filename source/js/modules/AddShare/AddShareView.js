define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    "use strict";
    return Backbone.NativeView.extend({
        tagName: 'div',
        className: 'shareOption',
        events: {
            'click .remove' : 'removeShare'
        },
        initialize: function () {
            this.render();
            this.model.on('change', this.render, this);
            this.model.on('remove destroy', this.remove, this);
            return this;
        },
        render: function () {
            this.template('', '', false).then(function (html) {
                this.el.innerHTML = html;
            }.bind(this));
            return this;
        },
        removeShare : function(){
            this.model.collection.remove(this.model);
        }
    });
});