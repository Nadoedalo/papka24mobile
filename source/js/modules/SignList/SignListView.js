define([
    'underscore',
    'backbone',
    'CryptoPlugin'
], function (_, Backbone) {
    "use strict";
    return Backbone.NativeView.extend({
        tagName: 'div',
        className: 'signList',
        events: {},
        initialize: function () {
            this.render();
            return this;
        },
        render: function () {
            this.template().then(function (html) {
                this.el.innerHTML = html;
            }.bind(this));
            return this;
        }
    });
});