define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    "use strict";
    return Backbone.NativeView.extend({
        tagName: 'div',
        className: 'document',
        events: {},
        initialize: function () {
            return this;
        },
        render: function () {
            this.template().then(function (html) {
                this.el.innerHTML = html;
            }.bind(this));
            return this;
        }
    });
});