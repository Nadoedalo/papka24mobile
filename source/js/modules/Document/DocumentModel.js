define([
    'underscore',
    'backbone',
    'js/modules/DocumentsCollection/DocumentsCollectionModel'//wrong inheritance
], function (_, Backbone, DocumentsCollectionModel) {
    "use strict";
    return DocumentsCollectionModel.extend({
        initialize: function () {
            this.fetch();
            this.fetchShares();
            //this.fetchSigns();
            return this;
        }
    });
});