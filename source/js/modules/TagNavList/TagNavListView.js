define([
    'underscore',
    'backbone',
    'js/app'
], function (_, Backbone, App) {
    "use strict";
    return Backbone.NativeView.extend({
        tagName: 'div',
        className: 'tagsNavList',
        events: {
            "click a" : 'handleHrefClick'
        },
        initialize: function () {
            this.render();
            this.model.on('change:tagList', this.render, this);
            return this;
        },
        render: function () {
            var data = {
                tags : this.model.get('tagList'),
                colors : ["-e57373","-f06292","-ba68c8","-9575cd","-7986cb","-4fc3f7","-4dd0e1","-4db6ac","-81c784","-aed581","-dce775","-fff176","-ffd54f","-ffb74d","-ff8a65","-90a4ae"]
            };
            this.template(data, this.className, false).then(function (html) {
                this.el.innerHTML = html;
                this.trigger('handleActive');
            }.bind(this), this._promiseErrorCatcher.bind(this));
            return this;
        },
        handleHrefClick : function(e){
            e.stopPropagation();
            e.preventDefault();
            var target = e.target,
                href = target.getAttribute('href');
            App.router.navigate(href, {trigger : true});
            document.querySelector('.menu.menu--visible').classList.remove('menu--visible');
        }
    });
});