/*jslint nomen:true, todo: true, unparam: true*/
define([
    'underscore',
    'backbone',
    'polyglot',
    'js/app',
    'require'
], function (_, Backbone, Polyglot, App, require) {
    "use strict";
    var extenstions = {
        _promiseId : 0,
        _promiseArr : [],
        /**
         * template prototype
         * @param data {Object}
         * @param name {String}
         * @param cache {Boolean}
         * @returns {Promise} //TODO
         */
        template: function (data, name, cache) {//requires name or className set
            /*
            * fixme refactor
            * can do :
            * var test = function(answer, method){method = method || 'resolve'; var promise = new $.Deferred(); console.log('here', answer); return promise[method]('a'+answer);};
            * test('voila').done(test).done(console.log.bind(console));
            * no more fallback hell
            * */
            /*
             * Возвращает promise с готовым HTML в done или fail если возникла ошибка
             * */
            return new window.Promise(function(resolveTemplate, rejectTemplate){
                data = data || (this.model ? _.clone(this.model.attributes) : undefined) || {};
                name = name || this.className || '';
                if(!name){throw new Error('No name specified for template');}
                var lang, // defaults to ru-RU
                    translate, // json-file must be reloaded on language change
                    template; // changed always
                    lang = App.userModel.get('userSettings').language || 'uk-UK';
                    cache = cache === undefined ? true : cache;
                    template = this.getTemplate(name);
                    translate = this.getTranslates(lang, name);
                window.Promise.all([template, translate])
                    .then(function(values){
                        var templateText = values[0],
                            translateText = values[1],
                            glot = this.glot || new Polyglot({
                                    warn : this.polyglotWarning
                                }),
                            securePromise;
                        glot.extend(translateText);
                        securePromise = this.secureTemplate(templateText, data, translateText);
                        this.glot = glot;
                        securePromise.then(function(html){
                            this.template = function (new_data) {
                                return new window.Promise(function(resolve){
                                    if (cache) {
                                        resolve(html);
                                    } else {
                                        new_data = _.extend(new_data, {polyglot: glot});
                                        resolve(this.secureTemplate(templateText, new_data, translateText));
                                    }
                                }.bind(this));
                            };
                            resolveTemplate(html);
                        }.bind(this), function(error){
                            error = {
                                error : error,
                                name : name
                            };
                            rejectTemplate(error);
                        });
                    }.bind(this));
            }.bind(this));
        },
        /**
         * Get Translate prototype
         * @param lang {String}
         * @param name {String}
         * @returns {*}
         */
        getTranslates: function (lang, name) {//requires name or className set
            lang = lang || App.userModel.get('userSettings').language || 'uk-UK';
            name = name || this.className || '';
            if (!name) {
                console.log('test here');
                throw new Error('No name specified');
            }
            this.glot = this.glot || new Polyglot({
                    warn : this.polyglotWarning
                });
            return new window.Promise(function(resolve){
                require(['text!/{{%VERSION%}}/html/languages/' + lang + '/' + name + '.json'], function (json) {
                    resolve(JSON.parse(json));
                }, function(){
                    console.log('test here 2');
                    throw new Error('Something went wrong with getting translates of '+name);
                });
            });
        },
        getTemplate : function(name){
            name = name || this.className || '';
            if (!name) {
                throw new Error('No name specified');
            }
            this.glot = this.glot || new Polyglot({
                    warn : this.polyglotWarning
                });
            return new window.Promise(function(resolve) {
                require(['text!/{{%VERSION%}}/html/' + name + '.html'], function (html) {
                    resolve(html);
                }, function () {
                    throw new Error('Something went wrong with getting html of ' + name);
                });
            });
        },
        polyglotWarning : function(message){ //mostly unnecessary function because of the sandbox
            console.warn('WARNING: ' + message);
        },
        secureTemplate : function(template, data, translate){
            var promise,
                worker = window._webWorker || new window.Worker('/lib/sandbox0.3.js'),
                promiseId = this._promiseId++,
                workerListener,
                removeListener = function(){
                    worker.removeEventListener('message', workerListener);
                };
            window._webWorker = worker;
            this.rejectPromises();
            promise = new window.Promise(function(resolve, reject){
                this._promiseArr.push({
                    resolve : resolve,
                    reject : reject
                });
                workerListener = function(e){
                    if(e.data.cid === this.cid && e.data.promiseId === promiseId){
                        resolve(e.data.template);
                    }
                }.bind(this);
                worker.addEventListener('message', workerListener);
                worker.postMessage(JSON.stringify({
                    template : template,
                    data : data,
                    translate : translate,
                    cid : this.cid,
                    promiseId : promiseId
                }));
            }.bind(this));
            promise.then(removeListener, removeListener);
            return promise;
        },
        rejectPromises : function(){
            _.invoke(this._promiseArr, 'reject', 'duplicate entry');
            this._promiseArr = [];
        },
        _promiseErrorCatcher : function(error){
                if(error.error !== 'duplicate entry'){
                    throw new Error(JSON.stringify(arguments));
                }
        }
    };
    _.extend(Backbone.NativeView.prototype, extenstions);
});