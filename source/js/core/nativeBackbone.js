/*jslint nomen:true, todo: true, unparam: true*/
define([
    'underscore',
    'backbone',
    'backboneNativeAjax',
    'backboneNativeView' //creates Backbone.NativeView
], function (_, Backbone, backboneNativeAjax) {
    "use strict";
    Backbone.ajax = backboneNativeAjax;
});