require.config({
    baseUrl: '/{{%VERSION%}}',
    paths: {
        polyglot: '/lib/polyglot.min',
        underscore: '/lib/underscore.1.8.3',
        backbone: '/lib/backbone.1.3.3',
        backboneNativeView : '/lib/BackboneNativeView',
        backboneNativeAjax : '/lib/BackboneNativeAjax',
        templateHelper: 'js/core/templateHelper',
        nativeBackbone : 'js/core/nativeBackbone',
        text: '/lib/text',
        md5 : '/lib/md5',
        sha256 : '/lib/sha256',
        fancyAutocomplete : '/lib/FancyAutocomplete',
        CryptoniteX : '/lib/CryptoniteX.min',
        CryptoPlugin : '/lib/CryptoPluginJS'
    },
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ['underscore'],
            exports: 'Backbone'
        },
        polyglot: {
            exports: 'Polyglot'
        },
        md5 : {
            exports : 'MD5'
        },
        sha256 : {
            exports : 'Sha256'
        },
        fancyAutocomplete : {
            deps : ['md5', 'sha256', 'underscore'],
            exports : 'FancyAutocomplete'
        },
        CryptoniteX : {
            exports : 'CryptoniteX'
        },
        CryptoPlugin : {
            deps : ['CryptoniteX'],
            exports : 'CryptoPluginJS'
        },
        backboneNativeView : {
            deps : ['backbone'],
            exports : 'Backbone.NativeView'
        },
        backboneNativeAjax : {
            deps : ['backbone'],
            exports : 'ajax'
        }
    },
    waitSeconds: 1500
});
require(['js/main']);